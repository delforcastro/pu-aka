# Pu'aka



## ¿Qué es Pu'aka?
Pu'aka es un proyecto web en desarrollo, usando HTML+PHP+BOOTSTRAP
El objetivo del proyecto es crear un sistema de gestión de clientes en entornos
de gimnasios.
Para este proyecto se está empleando CodeIgniter como framework para PHP.


## ¿Qué significa Pu'aka?

Pu'aka es un término guaraní. Significa "esfuerzo", "superación" o, más
genéricamente, "fuerza" (aquella fuerza que se realiza para lograr una meta)
