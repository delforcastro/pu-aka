<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>
        </div>

        <form class="" action="<?php echo base_url(); ?>/inmuebles/actualizar" method="post">
          <input type="hidden" name="id" id="id" value="<?php echo $inmueble['id']; ?>">

          <div class="form-group">
            <div class="row">
              <div class="col-12 col-sm-6">
                <label for="">Título</label>
                <input class="form-control" type="text" value="<?php echo $inmueble['titulo']; ?>" name="titulo" id="titulo" value="" autofocus required>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Descripción</label>
                <input class="form-control" type="text" value="<?php echo $inmueble['descripcion']; ?>" name="descripcion" id="descripcion" value="" required>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Tipo propiedad</label>
                <select class="form-control" name="tipopropiedad" id="tipopropiedad" required>
                  <option value="">Seleccionar tipo</option>
                  <?php
                    foreach ($tipopropiedad as $tpropiedad) { ?>

                      <option value="<?php echo $tpropiedad['id'] ?>" <?php if ($tpropiedad['id']==$inmueble['tipopropiedad']){ echo 'selected';} ?>><?php echo $tpropiedad['denominacion'] ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Tipo servicio</label>
                <select class="form-control" name="tiposervicio" id="tiposervicio" required>
                  <option value="">Seleccionar tipo</option>
                  <?php
                    foreach ($tiposervicio as $tservicio) { ?>

                      <option value="<?php echo $tservicio['id'] ?>" <?php if ($tservicio['id']==$inmueble['tiposervicio']){ echo 'selected';} ?>><?php echo $tservicio['denominacion'] ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Ciudad</label>
                <select class="form-control" name="ciudad" id="ciudad" required>
                  <option value="">Seleccionar ciudad</option>
                  <?php
                    foreach ($ciudades as $ciudad) { ?>

                      <option value="<?php echo $ciudad['id'] ?>" <?php if ($ciudad['id']==$inmueble['ciudad']){ echo 'selected';} ?>><?php echo $ciudad['denominacion'] ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Moneda</label>
                <select class="form-control" name="moneda" id="moneda" required>
                  <option value="">Seleccionar moneda</option>
                  <?php
                    foreach ($monedas as $moneda) { ?>

                      <option value="<?php echo $moneda['id'] ?>" <?php if ($moneda['id']==$inmueble['moneda']){ echo 'selected';} ?> ><?php echo $moneda['denominacion'] ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Valor</label>
                <input class="form-control" type="number" value="<?php echo $inmueble['valor']; ?>" name="valor" id="valor" value="" required>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Titular</label>
                <input class="form-control" type="text" value="<?php echo $inmueble['titular']; ?>" name="titular" id="titular" value="" required>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Destacado</label>
                <select class="form-control" name="destacado" id="destacado">
                  <option value="0" <?php if ($inmueble['destacado']== 0){ echo 'selected';} ?>>No</option>
                  <option value="1" <?php if ($inmueble['destacado']== 1){ echo 'selected';} ?>>Sí</option> 
                </select>
              </div>

            </div>
          </div>
            <a href="<?php echo base_url(); ?>/inmuebles" class="btn btn-primary">Volver</a>
            <button type="submit" name="button" class="btn btn-success">Guardar</button>

        </form>

    </main>
</div>
