
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>

            <div>
              <p>
                <a href="<?php echo base_url();?>/inmuebles/nuevo" class= "btn btn-info">Agregar</a>
                <a href="<?php echo base_url();?>/inmuebles/eliminadas" class= "btn btn-warning">Eliminadas</a>

              </p>
            </div>

                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Título</th>
                                <th>Tipo propiedad</th>
                                <th>Ciudad</th>
                                <th>Tipo servicio</th>
                                <th>Moneda</th>
                                <th>Valor</th>
                                <th>Titular</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                          <?php foreach ($datos as $dato) { ?>
                              <tr>
                                <td><?php echo $dato['id'];?></td>
                                <td><?php echo $dato['titulo'];?></td>
                                <td><?php echo $dato['tipopropiedad'];?></td>
                                <td><?php echo $dato['ciudad'];?></td>
                                <td><?php echo $dato['tiposervicio'];?></td>
                                <td><?php echo $dato['moneda'];?></td>
                                <td><?php echo $dato['valor'];?></td>
                                <td><?php echo $dato['titular'];?></td>
                                <td>
                                  <a href="<?php echo base_url();?>/inmuebles/editar/<?php echo $dato['id'];?>" class= "btn btn-primary">Editar</a>
                                </td>
                                <td>
                                  <a href="#" data-href="<?php echo base_url();?>/inmuebles/eliminar/<?php echo $dato['id'];?>" data-bs-toggle="modal" data-bs-target="#modalConfirma" class= "btn btn-danger">Eliminar</a>
                                  <!--
                                  <a href="<?php echo base_url();?>/inmuebles/eliminar/<?php echo $dato['id'];?>" class= "btn btn-danger">Eliminar</a>
                                -->
                                </td>
                              </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                </div>

    </main>
