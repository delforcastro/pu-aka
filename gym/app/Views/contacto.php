<div class="" style="background-color:red; margin-top:0px;">
  <h2>Contenido</h2>

<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#000" fill-opacity="1" d="M0,64L205.7,256L411.4,288L617.1,128L822.9,192L1028.6,32L1234.3,192L1440,288L1440,320L1234.3,320L1028.6,320L822.9,320L617.1,320L411.4,320L205.7,320L0,320Z"></path></svg>
</div>
<!-- Contact-->
<section id="contacto">

  <h1 class="section-header">Contáctanos</h1>

  <div class="contact-wrapper">

  <!-- Left contact page -->

    <form id="contact-form" class="form-horizontal" role="form">

      <div class="form-group">
        <div class="col">
          <input type="text" class="form-control" id="name" placeholder="NOMBRE" name="name" value="" required>
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-12">
          <input type="email" class="form-control" id="email" placeholder="EMAIL" name="email" value="" required>
          <textarea class="form-control" placeholder="MENSAJE" name="message" required></textarea>

          <button class="btn btn-primary send-button" id="submit" type="submit" value="SEND">
            <div class="alt-send-button">
              <i class="fa fa-paper-plane"></i><span class="send-text">ENVIAR</span>
            </div>

          </button>
        </div>




      </div>





    </form>

  <!-- Left contact page -->

      <div class="direct-contact-container">

        <ul class="contact-list">
          <li class="list-item"><i class="fa fa-map-marker fa-2x"><span class="contact-text place">Victoria, Entre Ríos</span></i></li>

          <li class="list-item"><i class="fa fa-phone fa-2x"><span class="contact-text phone"><a href="tel:1-212-555-5555" title="Llámanos">(3436) 15 - XXXXXX</a></span></i></li>

          <li class="list-item"><i class="fa fa-envelope fa-2x"><span class="contact-text gmail"><a href="mailto:#" title="Envíanos un email">info@desafiovictoria.com.ar</a></span></i></li>

        </ul>

        <hr>
        <ul class="social-media-list">
          <li><a href="#" target="_blank" class="contact-icon">
            <i class="fa fa-github" aria-hidden="true"></i></a>
          </li>
          <li><a href="#" target="_blank" class="contact-icon">
            <i class="fa fa-codepen" aria-hidden="true"></i></a>
          </li>
          <li><a href="#" target="_blank" class="contact-icon">
            <i class="fa fa-twitter" aria-hidden="true"></i></a>
          </li>
          <li><a href="#" target="_blank" class="contact-icon">
            <i class="fa fa-instagram" aria-hidden="true"></i></a>
          </li>
        </ul>
        <hr>

        <div class="copyright">&copy; Desarrollado con cariño por P&D Asesores</div>

      </div>

  </div>

</section>
