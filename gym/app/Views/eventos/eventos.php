
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>

            <div>
              <p>
                <a href="<?php echo base_url();?>/eventos/nuevo" class= "btn btn-info">Agregar</a>
                <a href="<?php echo base_url();?>/eventos/eliminadas" class= "btn btn-warning">Eliminados</a>

              </p>
            </div>

                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Denominación</th>
                                <th>Fecha</th>
                                <th>Acciones</th>

                            </tr>
                        </thead>

                        <tbody>
                          <?php foreach ($datos as $dato) { ?>
                              <tr>
                                <td><?php echo $dato['id'];?></td>
                                <td><?php echo $dato['denominacion'];?></td>
                                <td><?php echo $dato['fecha'];?></td>
                                <td>
                                  <a href="<?php echo base_url();?>/eventos/editar/<?php echo $dato['id'];?>" class= "btn btn-primary"><i class="bi bi-pencil"></i></a>
                                  <a href="#" data-href="<?php echo base_url();?>/eventos/eliminar/<?php echo $dato['id'];?>" data-bs-toggle="modal" data-bs-target="#modalConfirma" class= "btn btn-danger"><i class="bi bi-trash"></i></a>
                                </td>
                                
                              </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                </div>

    </main>
