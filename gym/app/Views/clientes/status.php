
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>
        </div>


        <div class="container-fluid px-4">



          <div class="row">
            <div class="col-sm-4 bg-primary">
              Apellido y Nombre:
            </div>
            <div class="col-sm-8 border">
              <?php
                echo $cliente['apellido'] . ", " . $cliente['nombre'];

               ?>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-4 bg-primary">
            Estado
            </div>
            <div class="col-sm-8 border">
              <?php
                if (empty($suscripciones)){
                  echo '<p class="text-danger"><i class="bi bi-shield-fill-x"></i>La persona no tiene suscripciones activas.</p>';
                }else{
                  echo '<p class="text-success"><i class="bi bi-shield-fill-check"></i>La persona tiene suscripciones activas.</p>';
                }
               ?>
            </div>
          </div>


           <h3>Suscripciones vigentes:</h3>
           <?php
           echo $cadena; ?>

           <table class="table">
             <thead>
               <tr>
                 <th>Servicio</th>
                 <th>Periodo alta</th>

               </tr>
             </thead>
             <tbody>
               <?php foreach ($suscripciones as $suscripcion) {
                    foreach ($servicios as $servicio) {
                        if ($servicio['id']==$suscripcion['gym_servicio']){
                          echo '<tr class="table-success">
                            <td>' .  $servicio['denominacion'] . '</td>
                            <td>'. $suscripcion['mes_alta'] . '-' . $suscripcion['year_alta'] . '</td>

                          </tr>';
                        }
                    }


                  }
                      ?>

             </tbody>
          </table>



        <table class="table">
          <thead>
            <tr>
              <th>Servicio</th>
              <th>Periodo</th>
              <th>Monto</th>
              <th>Vencimiento</th>
              <th>Estado</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($liquidaciones as $liquidacion) {

                         echo '<tr class="table-success">
                         <td>' .  $liquidacion['denominacion'] . '</td>
                         <td>' .  $liquidacion['mes'] . "-" . $liquidacion['year'] . '</td>
                         <td>'. $liquidacion['monto']  . '</td>
                         <td>'. date("d-m-Y", strtotime($liquidacion['fecha_vencimiento']))  . '</td>

                         <td>Pendiente</td>
                         <td>
                          <form action = "' . base_url() . '/pagos/nuevopago" method="post">

                            ' . csrf_field() . '
                            <input class="form-control" type="hidden" name="cliente" id="cliente" value="' . $cliente['id'] . '">
                            <input class="form-control" type="hidden" name="liquidacion" id="liquidacion" value="' . $liquidacion['tid'] . '">
                            <input class="form-control" type="hidden" name="monto" id="monto" value="' . $liquidacion['monto'] . '">
                            <input class="form-control" type="hidden" name="suscripcion" id="suscripcion" value="' . $liquidacion['susid'] . '">
                            <button type="submit" name="button" class="btn btn-success">Pagar</button>
                          </form>

                         </td>
                       </tr>';




               }
                   ?>

          </tbody>
       </table>
        </div>


    </main>
</div>
