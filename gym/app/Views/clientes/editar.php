
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>
        </div>
        <form class="" action="<?php echo base_url(); ?>/clientes/actualizar" method="post">
          <input type="hidden" name="id" id="id" value="<?php echo $datos['id']; ?>">
          <div class="form-group">
            <div class="row">
              <div class="col-12 col-sm-6">
                <label for="">Apellido</label>
                <input class="form-control" type="text" name="apellido" id="apellido" value="<?php echo $datos['apellido']; ?>" autofocus required>
              </div>
              <div class="col-12 col-sm-6">
                <label for="">Nombre</label>
                <input class="form-control" type="text" name="nombre" id="nombre" value="<?php echo $datos['nombre']; ?>" required>
              </div>
              <div class="col-12 col-sm-6">
                <label for="">Fecha nacimiento</label>
                <input class="form-control" type="date" name="fecha_nacimiento" id="fecha_nacimiento" value="<?php echo $datos['fecha_nacimiento']; ?>">
              </div>
              <div class="col-12 col-sm-6">
                <label for="">DNI</label>
                <input class="form-control" type="text" name="dni" id="dni" value="<?php echo $datos['dni']; ?>">
              </div>
              <div class="col-12 col-sm-6">
                <label for="">Grupo sanguíneo</label>
                <input class="form-control" type="text" name="grupo_sang" id="grupo_sang" value="<?php echo $datos['grupo_sang']; ?>">
              </div>
              <div class="col-12 col-sm-6">
                <label for="">Instagram</label>
                <input class="form-control" type="text" name="instagram" id="instagram" value="<?php echo $datos['instagram']; ?>">
              </div>
              <div class="col-12 col-sm-6">
                <label for="">Correo electrónico</label>
                <input class="form-control" type="text" name="email" id="email" value="<?php echo $datos['email']; ?>">
              </div>
              <div class="col-12 col-sm-6">
                <label for="">Teléfono</label>
                <input class="form-control" type="text" name="telefono" id="telefono" value="<?php echo $datos['telefono']; ?>">
              </div>


              <div class="col-12 col-sm-6">
                <label for="profesion">Profesión</label>
                <select class="form-control" name="profesion" id="profesion">
                  <option value="">(No establecida)</option>
                  <?php
                    foreach ($profesiones as $profesion) { ?>

                      <option value="<?php echo $profesion['id'] ?>"
                        <?php
                        if ($profesion['id'] == $datos['gym_profesion']){
                          echo 'selected';
                        }
                        ?>
                        ><?php echo $profesion['denominacion'] ?></option>
                  <?php } ?>

                </select>
              </div>
              <div class="col-12 col-sm-6">
                <label for="">Observaciones</label>
                <textarea class="form-control" name="obs" id="obs" rows="8" cols="80"><?php echo $datos['obs']; ?></textarea>
              </div>

            </div>
          </div>
            <a href="<?php echo base_url(); ?>/clientes" class="btn btn-primary">Volver</a>
            <button type="submit" name="button" class="btn btn-success">Actualizar</button>

        </form>

    </main>
</div>
