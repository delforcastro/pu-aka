
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>

            <div>
              <p>
                <a href="<?php echo base_url();?>/clientes/nuevo" class= "btn btn-info">Agregar</a>
                <a href="<?php echo base_url();?>/clientes/eliminadas" class= "btn btn-warning">Eliminadas</a>

              </p>
            </div>

                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Apellido</th>
                                <th>Nombre</th>
                                <th>DNI</th>
                                <th>Acciones</th>


                            </tr>
                        </thead>

                        <tbody>
                          <?php foreach ($datos as $dato) { ?>
                              <tr>
                                <td><?php echo $dato['id'];?></td>
                                <td><?php echo $dato['apellido'];?></td>
                                <td><?php echo $dato['nombre'];?></td>
                                <td><?php echo $dato['dni'];?></td>
                                <td>
                                  <a href="<?php echo base_url();?>/clientes/editar/<?php echo $dato['id'];?>" class= "btn btn-primary"><i class="bi bi-pencil"></i></a>
                                  <a href="<?php echo base_url();?>/clientes/status/<?php echo $dato['id'];?>" class= "btn btn-success"><i class="bi bi-eye"></i></a>
                                  <a href="<?php echo base_url();?>/pagos/nuevo/<?php echo $dato['id'];?>" class= "btn btn-warning"><i class="bi bi-currency-dollar"></i></a>
                                  <?php if ($admin==1){
                                    echo '<a href="#" data-href="' . base_url() . '/clientes/eliminar/' . $dato['id'] . '" data-bs-toggle="modal" data-bs-target="#modalConfirma" class= "btn btn-danger"><i class="bi bi-trash"></i></a>';
                                  }
                                  ?>

                                </td>

                              </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                </div>

    </main>
