
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>

            <div>
              <p>
                <a href="<?php echo base_url();?>/usuarios/nuevo" class= "btn btn-info">Agregar</a>
                <a href="<?php echo base_url();?>/usuarios/eliminadas" class= "btn btn-warning">Eliminadas</a>

              </p>
            </div>

                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Username</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                          <?php foreach ($datos as $dato) { ?>
                              <tr>
                                <td><?php echo $dato['id'];?></td>
                                <td><?php echo $dato['nombre'];?></td>
                                <td><?php echo $dato['user'];?></td>
                                <td>
                                  <a href="<?php echo base_url();?>/usuarios/editar/<?php echo $dato['id'];?>" class= "btn btn-primary">Editar</a>
                                </td>
                                <td>
                                  <a href="#" data-href="<?php echo base_url();?>/usuarios/eliminar/<?php echo $dato['id'];?>" data-bs-toggle="modal" data-bs-target="#modalConfirma" class= "btn btn-danger">Eliminar</a>

                                </td>
                              </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                </div>

    </main>
