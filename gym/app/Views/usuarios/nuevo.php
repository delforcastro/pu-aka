
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>
        </div>

        <form class="" action="<?php echo base_url(); ?>/usuarios/insertar" method="post">
          <div class="form-group">
            <div class="row">
              <div class="col-12 col-sm-6">
                <label for="">Nombre</label>
                <input class="form-control" type="text" name="nombre" id="nombre" value="" autofocus required>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Username</label>
                <input class="form-control" type="text" name="user" id="user" value="" required>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Contraseña</label>
                <input class="form-control" type="password" name="pss" id="pss" value="" required>
              </div>
            </div>



          </div>
            <a href="<?php echo base_url(); ?>/usuarios" class="btn btn-primary">Volver</a>
            <button type="submit" name="button" class="btn btn-success">Guardar</button>

        </form>

    </main>
</div>
