<div id="layoutSidenav_content">
  <main>
      <div class="container-fluid">
        <h1>Hola, bienvenido.</h1>
        <div class="row">
          <div class="col-md-3 text-center p-1 m-1">
            <a class="btn btn-primary" style="width: 100%;" href="<?php echo base_url(); ?>/clientes"><i class="bi bi-file-earmark-person"></i></i> <br>Clientes
              <br>
              <hr>
              <p> <?php echo $tclientes . " clientes registrados"; ?></p>
            </a>

          </div>

          <div class="col-md-3 text-center p-1 m-1">
            <a class="btn btn-success" style="width: 100%;" href="<?php echo base_url(); ?>/clientes"><i class="bi bi-calendar"></i><br>Cumpleaños
              <br>
              <hr>
              <p> <?php echo $tcumple . " cumpleaños en este mes"; ?></p>
            </a>

          </div>

          <div class="col-md-3 text-center p-1 m-1">
            <a class="btn btn-secondary" style="width: 100%;" href="<?php echo base_url(); ?>/clientes"><i class="bi bi-calendar"></i><br>Cumpleaños
              <br>
              <hr>
              <p> <?php echo $tcumple . " cumpleaños en este mes"; ?></p>
            </a>

          </div>



        </div>
      </div>
  </main>
