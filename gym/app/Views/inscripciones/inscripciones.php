
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>

            <div>
              <p>
                <a href="<?php echo base_url();?>/inscripciones/nuevo" class= "btn btn-info">Agregar</a>
                <a href="<?php echo base_url();?>/inscripciones/eliminadas" class= "btn btn-warning">Eliminadas</a>

              </p>
            </div>

                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Apellido</th>
                                <th>Nombre</th>
                                <th>Ciudad</th>
                                <th>Provincia</th>
                                <th>Distancia</th>
                                <th>Categoria</th>
                                <th>DNI</th>
                                <th>Talle</th>
                                <th>Acciones</th>

                            </tr>
                        </thead>

                        <tbody>
                          <?php foreach ($datos as $dato) { ?>
                              <tr <?php if ($dato['pago'] == 1){ echo 'style="background-color:#7ea866;"'; } ?>>
                                <td><?php echo $dato['id'];?></td>
                                <td><?php echo $dato['apellido'];?></td>
                                <td><?php echo $dato['nombre'];?></td>

                                <td><?php echo $dato['ciudad'];?></td>
                                <td><?php foreach ($provincias as $provincia) {
                                  if ($provincia['id'] == $dato['provincia']){
                                    echo $provincia['denominacion'];

                                  }
                                } ?></td>

                                <td><?php foreach ($distancias as $distancia) {
                                  if ($distancia['id'] == $dato['distancia']){
                                    echo $distancia['denominacion'];

                                  }
                                } ?></td>

                                <td><?php foreach ($categorias as $categoria) {
                                  if ($categoria['id'] == $dato['categoria']){
                                    echo $categoria['denominacion'];

                                  }
                                } ?></td>

                                <td><?php echo $dato['dni'];?></td>

                                <td><?php foreach ($talles as $talle) {
                                  if ($talle['id'] == $dato['talle']){
                                    echo $talle['denominacion'];

                                  }
                                } ?></td>
                                <td>
                                  <a href="<?php echo base_url();?>/inscripciones/editar/<?php echo $dato['id'];?>" class= "btn btn-primary"><i class="bi bi-pencil"></i></a>
                                  <a href="#" data-href="<?php echo base_url();?>/inscripciones/eliminar/<?php echo $dato['id'];?>" data-bs-toggle="modal" data-bs-target="#modalConfirma" class= "btn btn-danger"><i class="bi bi-trash"></i></a>
                                  <a href="#" data-href="<?php echo base_url();?>/inscripciones/pagar/<?php echo $dato['id'];?>" data-bs-toggle="modal" data-bs-target="#modalConfirmaPago" class= "btn btn-warning"><i class="bi bi-currency-dollar"></i></a>
                                </td>

                              </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                </div>

    </main>
