
<div id="forminscripcionweb">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>
        </div>

        <form class="form" action="<?php echo base_url(); ?>/inscripciones/insertarweb" method="post">
          <div class="form-group">
            <div class="row">

              <div class="col-12 col-sm-6">
                <label for="">Evento</label>
                <select class="form-control" name="evento" id="evento" required>
                  <option value="">Seleccionar evento</option>
                  <?php
                    foreach ($eventos as $evento) { ?>

                      <option value="<?php echo $evento['id'] ?>"><?php echo $evento['denominacion'] ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Distancia</label>
                <select class="form-control" name="distancia" id="distancia" required>
                  <option value="">Seleccionar distancia</option>
                  <?php
                    foreach ($distancias as $distancia) { ?>

                      <option value="<?php echo $distancia['id'] ?>"><?php echo $distancia['denominacion'] ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Apellido</label>
                <input class="form-control" type="text" name="apellido" id="apellido" value="" required>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Nombre</label>
                <input class="form-control" type="text" name="nombre" id="nombre" value="" required>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">DNI (sin puntos ni espacios)</label>
                <input class="form-control" type="number" name="dni" id="dni" value="" required>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Sexo</label>
                <select class="form-control" name="sexo" id="sexo">
                  <option value="0">Femenino</option>
                  <option value="1">Masculino</option>
                </select>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Fecha de nacimiento</label>
                <input class="form-control" type="date" name="fecha_nacimiento" id="fecha_nacimiento" value="" required>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Categoría</label>
                <select class="form-control" name="categoria" id="categoria" required>
                  <option value="">Seleccionar categoría</option>
                  <?php
                    foreach ($categorias as $categoria) { ?>

                      <option value="<?php echo $categoria['id'] ?>"><?php echo $categoria['denominacion'] ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Teléfono</label>
                <input class="form-control" type="tel" name="telefono" id="telefono" value="" required>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Ciudad</label>
                <input class="form-control" type="text" name="ciudad" id="ciudad" value="" required>
              </div>


              <div class="col-12 col-sm-6">
                <label for="">Provincia</label>
                <select class="form-control" name="provincia" id="provincia" required>
                  <option value="">Seleccionar provincia</option>
                  <?php
                    foreach ($provincias as $provincia) { ?>

                      <option value="<?php echo $provincia['id'] ?>"><?php echo $provincia['denominacion'] ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Talle</label>
                <select class="form-control" name="talle" id="talle" required>
                  <option value="">Seleccionar talle</option>
                  <?php
                    foreach ($talles as $talle) { ?>

                      <option value="<?php echo $talle['id'] ?>"><?php echo $talle['denominacion'] ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Email</label>
                <input class="form-control" type="email" name="email" id="email" value="" required>
              </div>

            </div>
          </div>


          <div class="text-center">
                <button type="submit" name="button" style="align:right;" class="btn btn-success">¡Inscribirme!</button>
                
              </div>

        </form>

    </main>

</div>

<h4 class="section-header" style="color:#b59824;">Lista de inscriptos</h4>
<div id="gridinscripcion">
    <main>
        <div class="container-fluid px-4">


                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Apellido</th>
                                <th>Nombre</th>
                                <th>Ciudad</th>
                                <th>Provincia</th>
                                <th>Distancia</th>
                                <th>Categoria</th>


                            </tr>
                        </thead>

                        <tbody>
                          <?php foreach ($datos as $dato) { ?>
                              <tr>
                                <td><?php echo $dato['id'];?></td>
                                <td><?php echo $dato['apellido'];?></td>
                                <td><?php echo $dato['nombre'];?></td>

                                <td><?php echo $dato['ciudad'];?></td>
                                <td><?php foreach ($provincias as $provincia) {
                                  if ($provincia['id'] == $dato['provincia']){
                                    echo $provincia['denominacion'];

                                  }
                                } ?></td>

                                <td><?php foreach ($distancias as $distancia) {
                                  if ($distancia['id'] == $dato['distancia']){
                                    echo $distancia['denominacion'];

                                  }
                                } ?></td>

                                <td><?php foreach ($categorias as $categoria) {
                                  if ($categoria['id'] == $dato['categoria']){
                                    echo $categoria['denominacion'];

                                  }
                                } ?></td>
                              </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                </div>

    </main>
  </div>
