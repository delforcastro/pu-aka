<<?php
  $user_session = session();
 ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>DesafíoVictoria</title>




        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css" />
        <!-- SimpleLightbox plugin CSS-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/SimpleLightbox/2.1.0/simpleLightbox.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css">
        <!-- iconografía de BS -->

        <link href="<?php echo base_url();?>/templates/css/bootstrap-icons.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<?php echo base_url();?>/templates/css/styles.css" rel="stylesheet" />
        <link href="<?php echo base_url();?>/templates/css/redes.css" rel="stylesheet" />
        <link href="<?php echo base_url();?>/templates/css/login.css" rel="stylesheet" />
        <link href="<?php echo base_url();?>/templates/css/icons.css" rel="stylesheet" />





        <link href="<?php echo base_url();?>/css/datatables.css" rel="stylesheet" />
        <link href="<?php echo base_url();?>/css/estilosbtemplate.css" rel="stylesheet" />

        <script src="<?php echo base_url();?>/js/fontawesome.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>


        <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->

    </head>



        <body class="sb-nav-fixed">
          <nav class="sb-topnav navbar navbar-expand navbar-dark bg-primary">
          <!-- Navbar Brand-->
          <a class="navbar-brand ps-3" href="index.html">Gym Vic30</a>
          <!-- Sidebar Toggle-->
          <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>

          <!-- Navbar-->
          <ul class="navbar-nav d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"> <?php echo $user_session->nombre; ?><i class="fas fa-user fa-fw"></i></a>
                  <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                      <li><a class="dropdown-item" href="<?php echo base_url(); ?>/usuarios/cambiapassword">Cambiar contraseña</a></li>

                      <li><hr class="dropdown-divider" /></li>
                      <li><a class="dropdown-item" href="<?php echo base_url(); ?>/login/logout">Cerrar sesión</a></li>
                  </ul>
              </li>
          </ul>

        </nav>
            <div id="layoutSidenav">
                <div id="layoutSidenav_nav">
                    <nav class="sb-sidenav accordion sb-sidenav-primary" id="sidenavAccordion">
                        <div class="sb-sidenav-menu">

                            <div class="nav">
                              <a class="nav-link" href="<?php echo base_url(); ?>/panel">
                                  <div class="sb-nav-link-icon"><i class="fa fa-th-large"></i></div>
                                  Panel

                              </a>

                            </div>
                            <div class="nav">



                                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                    <div class="sb-nav-link-icon"><i class="fa fa-building"></i></div>
                                    Gestión
                                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                </a>
                                <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                    <nav class="sb-sidenav-menu-nested nav">
                                        <a class="nav-link" href="<?php echo base_url(); ?>/clientes"><b>Clientes</b></a>
                                        <a class="nav-link" href="<?php echo base_url(); ?>/pagos"><b>Pagos</b></a>
                                        <a class="nav-link" href="<?php echo base_url(); ?>/servicios">Servicios</a>
                                        <a class="nav-link" href="<?php echo base_url(); ?>/suscripciones">Suscripciones</a>
                                        <a class="nav-link" href="<?php echo base_url(); ?>/tarifas">Tarifas</a>
                                        <a class="nav-link" href="<?php echo base_url(); ?>/provincias">Provincias</a>
                                        <a class="nav-link" href="<?php echo base_url(); ?>/categorias">Categorías</a>
                                        <a class="nav-link" href="<?php echo base_url(); ?>/distancias">Distancias</a>
                                        <a class="nav-link" href="<?php echo base_url(); ?>/eventos">Eventos</a>
                                        <a class="nav-link" href="<?php echo base_url(); ?>/inscripciones">Inscripciones</a>
                                        <a class="nav-link" href="<?php echo base_url(); ?>/profesiones">Profesiones</a>
                                    </nav>
                                </div>




                            </div>


                            <?php if ($admin==1){
                              echo '<div class="nav">
                                  <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts2" aria-expanded="false" aria-controls="collapseLayouts">
                                      <div class="sb-nav-link-icon"><i class="fa fa-wrench"></i></div>
                                      Configuración
                                      <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                  </a>
                                  <div class="collapse" id="collapseLayouts2" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                      <nav class="sb-sidenav-menu-nested nav">
                                        <a class="nav-link" href="' . base_url() . '/usuarios">Usuarios</a>

                                      </nav>
                                  </div>
                              </div>';
                            } ?>


                        </div>
                        <div class="sb-sidenav-footer">
                            <div class="small"></div>

                        </div>
                    </nav>
                </div>
