<?php include_once 'header.php' ?>

<body>
    <div class="container-fluid">



        <nav class="navbar navbar-dark navbar-expand-lg bg-dark">
          <a class="navbar-brand" href="#">Gym Vic30</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a class="nav-link" href=" <?php echo base_url() . 'admin/logout'  ?> "><i class="gg-log-off"></i>Cerrar sesión</a>
              </li>
            </ul>
          </div>
        </nav>

        <div class="row">
          <div class="col">
              <h3>Panel de administración</h3>
          </div>
        </div>

        <div class="row">
          <div class="col">


            <!-- Tarjeta de ítems-->
            <div class="card" style="width: 18rem;">
              <img src="templates/img/fondoInmo.png" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Ítems</h5>
                <p class="card-text">Ingrese aquí para administrar los ítems disponibles en la web.</p>
                <a href=" <?php echo base_url() . 'admin/items'  ?> " class="btn btn-primary">Ingresar</a>
              </div>
            </div>
          </div>

            <!-- Tarjeta de ciudades-->
            <div class="col">
            <div class="card" style="width: 18rem;">
              <img src="templates/img/fondoInmo.png" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Ciudades</h5>
                <p class="card-text">Ingrese aquí para administrar las ciudades/zonas disponibles en la web.</p>
                <a href=" <?php echo base_url() . 'admin/ciudades'  ?> " class="btn btn-primary">Nueva</a>
                <a href=" <?php echo base_url() . 'admin/listado_ciudades'  ?> " class="btn btn-secondary">Ver todas</a>
              </div>
            </div>


        </div>

        <!-- Tarjeta de monedas-->
        <div class="col">
        <div class="card" style="width: 18rem;">
          <img src="templates/img/fondoInmo.png" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">Monedas</h5>
            <p class="card-text">Ingrese aquí para administrar las monedas disponibles en la web.</p>
            <a href=" <?php echo base_url() . 'admin/monedas'  ?> " class="btn btn-primary">Nueva</a>
            <a href=" <?php echo base_url() . 'unidades'  ?> " class="btn btn-secondary">Ingresar</a>
          </div>
        </div>


    </div>
      </div>
    </div>

 <?php include_once 'footer.php' ?>
