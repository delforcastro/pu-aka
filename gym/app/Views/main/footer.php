

<!-- Footer-->
<footer class="bg-light py-5">
		<div class="container px-4 px-lg-5"><div class="small text-center text-muted">Copyright &copy; 2021 <br>Desarrollado con cariño por <a href="#">P&D Asesores</a></div> </div>
</footer>
<!-- Bootstrap core JS-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
<!-- SimpleLightbox plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/SimpleLightbox/2.1.0/simpleLightbox.min.js"></script>
<script src="<?php echo base_url();?>/js/simple-datatables@latest.js"></script>
<script src="<?php echo base_url();?>/js/datatables-simple-demo.js"></script>

<!-- Core theme JS-->
<script src="<?= base_url() ?>templates/js/scripts.js"></script>
<script src="<?= base_url() ?>templates/js/js_custom.js"></script>


<script>
  $('#modalConfirmaWeb').on('show.bs.modal', function(e){
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

  });

</script>

</body>
</html>
