
<body id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
        <div class="container px-4 px-lg-5" style="display:flex;justify-content: left;align-items: center;">
            <a class="navbar-brand" href="#page-top">Villanueva<br>Inmobiliaria</a>
            <img src="templates/img/logoweb.png" alt="" style="width:110px;">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>


            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ms-auto my-2 my-lg-0">
                    <li class="nav-item"><a class="nav-link" href="#about">Nuestra empresa</a></li>
                    <li class="nav-item"><a class="nav-link" href="#services">Servicios</a></li>
                    <li class="nav-item"><a class="nav-link" href="#contact">Contacto</a></li>

                    <!--
                    <div class="divJuga">
                      <a class="btnJuga btnJuga2" href="http://www.dhcinformatica.com.ar" target="_blank">
                        <div>Servicios</div>
                      </a>
                    </div>
                    <div class="divJuga">
                      <a class="btnJuga btnJuga1" href="http://www.dhcinformatica.com.ar" target="_blank">

                        <div>Contáctanos</div>
                      </a>
                    </div>
                </ul>
            </div>
          -->
            <!-- METO STYLES-->
                <style>
                .divJuga {
                display: flex;
                justify-content: center;
                padding-left: 37px;
                margin-right: 15px;
                }

                .btnJuga1
                {
                  background: linear-gradient(-90deg, #EE7752, #E73C77, #E3A6D5, #23D5AB, #EE7752);
                }

                .btnJuga2{
                    background: linear-gradient(-45deg, #AE7752, #AE7752, #23A6D5, #AE7752, #EE7752);
                }

                .btnJuga {
                position: relative;
                background-size: 400% 100%;
                /*text-transform: uppercase;*/
                font-weight: 500;
                border: none;
                border-radius: 0.25em;
                font-size: 19px;
                letter-spacing: 1px;
                padding: 0;
                margin: 0;
                animation: Gradient 4s ease infinite;
                text-decoration: none;
                /*padding-left:10px;
                padding-right:10px;*/
                }

                .btnJuga div {
                color: white;
                /*min-height: 40px;
                padding: 0 1.618em;*/
                border-radius: inherit;
                background-size: inherit;
                background-image: inherit;
                animation: Gradient 9s ease infinite;
                position: relative;
                z-index: 1;
                mix-blend-mode: overlay;
                font-family: 'Roboto';
                padding-top:10px;
                padding-bottom:10px;
                padding-left:17px;
                padding-right:17px;
                font-family: "Oswald", sans-serif;
                }

                @keyframes  Gradient {
                50% {
                  background-position: 100% 60%;
                  transform: skew(-2deg);
                }
                }

                .btnJuga::after {
                content: '';
                position: absolute;
                background-size: inherit;
                background-image: inherit;
                animation: inherit;
                left: 0px;
                right: 0px;
                top: 2px;
                height: 100%;
                filter: blur(1rem);
                }

                </style>

            <!--fin styles -->
        </div>
      </div>
    </nav>
    <a href="https://api.whatsapp.com/send?phone=+5493436571327" class="btn-wsp" target="_blank">
	    <i class="bi bi-whatsapp"></i>
	</a>
