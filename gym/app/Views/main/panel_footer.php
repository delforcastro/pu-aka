<footer class="py-4 bg-light mt-auto">
    <div class="container-fluid px-4">
        <div class="d-flex align-items-center justify-content-between small">
            <div class="text-muted">Desarrollado con cariño por PyD Asesores. <br>
              <?php
                echo "Fecha: " . date('d-m-Y');
               ?>
            </div>

        </div>
    </div>
</footer>
</div>
</div>
<script src="<?php echo base_url();?>/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url();?>/js/scriptsbtemplate.js"></script>
<script src="<?php echo base_url();?>/js/simple-datatables@latest.js"></script>
<script src="<?php echo base_url();?>/js/datatables-simple-demo.js"></script>

<!-- Bootstrap core JS-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
<!-- SimpleLightbox plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/SimpleLightbox/2.1.0/simpleLightbox.min.js"></script>


<script>
  $('#modalConfirma').on('show.bs.modal', function(e){
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

  });

</script>

<script>
  $('#modalConfirmaPago').on('show.bs.modal', function(e){
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

  });

</script>

</body>
</html>
