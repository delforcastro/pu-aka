
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>

            <div>
              <p>
                <a href="<?php echo base_url();?>/servicios/nuevo" class= "btn btn-info">Agregar</a>
                <a href="<?php echo base_url();?>/servicios/eliminadas" class= "btn btn-warning">Eliminadas</a>

              </p>
            </div>

                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Denominación</th>
                                <th>Acciones</th>

                            </tr>
                        </thead>

                        <tbody>
                          <?php foreach ($datos as $dato) { ?>
                              <tr>
                                <td><?php echo $dato['id'];?></td>
                                <td><?php echo $dato['denominacion'];?></td>
                                <td>

                                  <?php if ($admin==1){
                                  echo '<a href="' . base_url() . '/servicios/editar/' . $dato['id'] . '" class= "btn btn-primary"><i class="bi bi-pencil"></i></a>
                                  <a href="#" data-href="' . base_url() . '/servicios/eliminar/' . $dato['id'] . '" data-bs-toggle="modal" data-bs-target="#modalConfirma" class= "btn btn-danger"><i class="bi bi-trash"></i></a>';
                                    }
                                  ?>
                                </td>

                              </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                </div>

    </main>
