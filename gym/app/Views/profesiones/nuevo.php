
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>
        </div>
        <?php \Config\Services::validation()->listErrors(); ?>
        <form class="" action="<?php echo base_url(); ?>/profesiones/insertar" method="post">
          <?php csrf_field(); ?>
          <div class="form-group">
            <div class="row">
              <div class="col-12 col-sm-6">
                <label for="">Denominación</label>
                <input class="form-control" type="text" name="denominacion" id="denominacion" value="" autofocus required>
              </div>
            </div>
            <div class="row">


              <div class="col-12 col-sm-6">
                <label for="">Día</label>
                <input class="form-control" type="text" name="dia" id="dia" value="">
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Mes</label>
                <input class="form-control" type="text" name="mes" id="mes" value="">
              </div>
            </div>    

          </div>
            <a href="<?php echo base_url(); ?>/profesiones" class="btn btn-primary">Volver</a>
            <button type="submit" name="button" class="btn btn-success">Guardar</button>

        </form>

    </main>
</div>
