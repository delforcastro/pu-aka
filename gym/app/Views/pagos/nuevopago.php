
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>

            <?php \Config\Services::validation()->listErrors(); ?>
            <form class="" action="<?php echo base_url(); ?>/pagos/insertarpago" method="post">
              <?php csrf_field(); ?>
              <div class="form-group">
                <div class="row">

                    <input type="hidden" name="cliente" id="cliente" value="<?php echo $cliente['id']; ?>">

                    <input type="hidden" name="suscripcion" id="suscripcion" value="<?php echo $suscripcion['id']; ?>">

                    <input type="hidden" name="tarifa" id="tarifa" value="<?php echo $tarifa['id']; ?>">
                    <h3>Datos del pago</h3>

                    <div class="row">
                      <div class="col-sm-4 bg-primary">
                        Apellido y Nombre:
                      </div>
                      <div class="col-sm-8 border">
                        <?php
                          echo $cliente['apellido'] . ", " . $cliente['nombre'];

                         ?>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-4 bg-primary">
                        Servicio
                      </div>
                      <div class="col-sm-8 border">
                        <?php
                          echo $servicio['denominacion'];

                         ?>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-4 bg-primary">
                        Periodo
                      </div>
                      <div class="col-sm-8 border">
                        <?php
                          echo $tarifa['mes'] . "-" . $tarifa['year'];

                         ?>
                      </div>

                    </div>

                    <div class="row">
                      <div class="col-sm-4 bg-primary">
                        Monto
                      </div>
                      <div class="col-sm-8 border">
                        <input type="text" name="monto" id="monto" value="<?php echo $tarifa['monto']; ?>">
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-4 bg-primary">
                        Fecha de pago
                      </div>
                      <div class="col-sm-8 border">
                        <input type="date" name="fecha_pago" id="fecha_pago" value="<?php echo date('Y-m-d'); ?>">
                      </div>
                    </div>

                </div>
              </div>
                <a href="<?php echo base_url(); ?>/clientes/status/ <?php echo $cliente['id']; ?>" class="btn btn-primary">Volver</a>
                <button type="submit" name="button" class="btn btn-success">Guardar</button>

            </form>


        </div>

    </main>
</div>
