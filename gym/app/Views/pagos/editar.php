
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>
        </div>

        <form class="" action="<?php echo base_url(); ?>/servicios/actualizar" method="post">
          <input type="hidden" name="id" id="id" value="<?php echo $datos['id']; ?>">
          <div class="form-group">
            <div class="row">

              <div class="col-12 col-sm-6">
                <label for="profesion">Suscripción</label>
                <select class="form-control" name="suscripcion" id="suscripcion">

                  <?php
                    foreach ($suscripciones as $suscripcion) { ?>

                      <option value="<?php echo $suscripcion['id'] ?>"><?php echo 'Cliente ' . $suscripcion['cliente'] . ' | Servicio: ' . $suscripcion['gym_servicio']; ?></option>
                  <?php } ?>

                </select>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Monto</label>
                <input class="form-control" type="text" name="monto" id="monto" value="">
              </div>
            </div>
          </div>
            <a href="<?php echo base_url(); ?>/pagos" class="btn btn-primary">Volver</a>
            <button type="submit" name="button" class="btn btn-success">Actualizar</button>

        </form>

    </main>
</div>
