
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>

            <div>
              <p>
                <a href="<?php echo base_url();?>/pagos/nuevo" class= "btn btn-info">Agregar</a>
                <a href="<?php echo base_url();?>/pagos/eliminadas" class= "btn btn-warning">Eliminadas</a>

              </p>
            </div>

                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Suscripción</th>
                                <th>Monto</th>
                                <th>Cliente</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>

                        <tbody>
                          <?php foreach ($datos as $dato) { ?>
                              <tr>
                                <td><?php echo $dato['id'];?></td>
                                <td><?php echo $dato['suscripcion'];?></td>
                                <td><?php echo $dato['monto'];?></td>
                                <td><?php echo $dato['apellido'] . ", " . $dato['nombre'];?></td>
                                <td>

                                  <?php if ($admin==1){
                                  echo '<a href="' . base_url() . '/pagos/editar/' . $dato['id'] . '" class= "btn btn-primary"><i class="bi bi-pencil"></i></a>
                                  <a href="#" data-href="' . base_url() . '/pagos/eliminar/' . $dato['id'] . '" data-bs-toggle="modal" data-bs-target="#modalConfirma" class= "btn btn-danger"><i class="bi bi-trash"></i></a>';
                                    }
                                  ?>
                                </td>

                              </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                </div>

    </main>
