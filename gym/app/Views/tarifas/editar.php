<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>
        </div>

        <form class="" action="<?php echo base_url(); ?>/tarifas/actualizar" method="post">
          <input type="hidden" name="id" id="id" value="<?php echo $datos['id']; ?>">

          <div class="form-group">
            <div class="row">

              <div class="col-12 col-sm-6">
                <label for="">Servicio</label>
                <select class="form-control" name="servicio" id="servicio" required>
                  <option value="">Seleccionar servicio</option>
                  <?php
                    foreach ($servicios as $servicio) { ?>

                      <option value="<?php echo $servicio['id'] ?>" <?php if ($servicio['id']==$datos['servicio']){ echo 'selected';} ?>><?php echo $servicio['denominacion'] ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Mes</label>
                <select class="form-control" name="mes" id="mes" required>
                  <option value="">Seleccione mes</option>
                    <option value="1" <?php if ($datos['mes']== 1){ echo 'selected';} ?>>Enero</option>
                    <option value="2" <?php if ($datos['mes']== 2){ echo 'selected';} ?>>Febrero</option>
                    <option value="3" <?php if ($datos['mes']== 3){ echo 'selected';} ?>>Marzo</option>
                    <option value="4" <?php if ($datos['mes']== 4){ echo 'selected';} ?>>Abril</option>
                    <option value="5" <?php if ($datos['mes']== 5){ echo 'selected';} ?>>Mayo</option>
                    <option value="6" <?php if ($datos['mes']== 6){ echo 'selected';} ?>>Junio</option>
                    <option value="7" <?php if ($datos['mes']== 7){ echo 'selected';} ?>>Julio</option>
                    <option value="8" <?php if ($datos['mes']== 8){ echo 'selected';} ?>>Agosto</option>
                    <option value="9" <?php if ($datos['mes']== 9){ echo 'selected';} ?>>Septiembre</option>
                    <option value="10" <?php if ($datos['mes']== 10){ echo 'selected';} ?>>Octubre</option>
                    <option value="11" <?php if ($datos['mes']== 11){ echo 'selected';} ?>>Noviembre</option>
                    <option value="12" <?php if ($datos['mes']== 12){ echo 'selected';} ?>>Diciembre</option>

                </select>
              </div>
              <div class="col-12 col-sm-6">
                <label for="">Año</label>
                <select class="form-control" name="year" id="year" required>
                  <option value="">Seleccione año</option>
                    <option value="2022" <?php if ($datos['year']=='2022'){ echo 'selected';} ?>>2022</option>
                    <option value="2023" <?php if ($datos['year']=='2023'){ echo 'selected';} ?>>2023</option>
                    <option value="2024" <?php if ($datos['year']=='2024'){ echo 'selected';} ?>>2024</option>

                </select>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Monto</label>
                <input class="form-control" type="text" name="monto" id="monto" value="<?php echo $datos['monto']; ?>" required>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Fecha de vencimiento</label>
                <input class="form-control" type="date" name="fecha_vencimiento" id="fecha_vencimiento" value="<?php echo $datos['fecha_vencimiento']; ?>">
              </div>

            </div>
          </div>
            <a href="<?php echo base_url(); ?>/tarifas" class="btn btn-primary">Volver</a>
            <button type="submit" name="button" class="btn btn-success">Guardar</button>

        </form>

    </main>
</div>
