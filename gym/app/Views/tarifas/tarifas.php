
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>

            <div>
              <p>
                <a href="<?php echo base_url();?>/tarifas/nuevo" class= "btn btn-info">Agregar</a>
                <a href="<?php echo base_url();?>/tarifas/eliminadas" class= "btn btn-warning">Eliminadas</a>

              </p>
            </div>

                    <table id="datatablesSimple">
                      <thead>
                        <tr>
                            <th>ID</th>
                            <th>Servicio</th>
                            <th>Periodo</th>
                            <th>Monto</th>
                            <th>Vencimiento</th>
                            <th>Acciones</th>

                        </tr>
                      </thead>

                      <tbody>
                        <?php foreach ($datos as $dato) { ?>
                            <tr>
                              <td><?php echo $dato['id'];?></td>
                              <td><?php foreach ($servicios as $servicio) {
                                if ($servicio['id'] == $dato['servicio']){
                                  echo $servicio['denominacion'];

                                }
                              } ?></td>
                              <td><?php echo $dato['mes'] . "-" . $dato['year'];?></td>
                              <td><?php echo $dato['monto'];?></td>
                              <td><?php echo $dato['fecha_vencimiento'];?></td>
                                <td>
                                  <a href="<?php echo base_url();?>/tarifas/editar/<?php echo $dato['id'];?>" class= "btn btn-primary"><i class="bi bi-pencil"></i></a>
                                  <a href="#" data-href="<?php echo base_url();?>/tarifas/eliminar/<?php echo $dato['id'];?>" data-bs-toggle="modal" data-bs-target="#modalConfirma" class= "btn btn-danger"><i class="bi bi-trash"></i></a>
                                </td>

                              </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                </div>

    </main>
