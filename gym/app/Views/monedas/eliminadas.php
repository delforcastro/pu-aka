
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>

            <div>
              <p>
                <a href="<?php echo base_url();?>/monedas" class= "btn btn-warning">Volver a Monedas</a>

              </p>
            </div>

                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Denominación</th>
                                <th>Símbolo</th>
                                <th></th>

                            </tr>
                        </thead>

                        <tbody>
                          <?php foreach ($datos as $dato) { ?>
                              <tr>
                                <td><?php echo $dato['id'];?></td>
                                <td><?php echo $dato['denominacion'];?></td>
                                <td><?php echo $dato['simbolo'];?></td>

                                <td>
                                  <a href="#" data-href="<?php echo base_url();?>/monedas/restaurar/<?php echo $dato['id'];?>" data-bs-toggle="modal" data-bs-target="#modalConfirma" class= "btn btn-danger">Restaurar</a>
                                </td>
                              </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                </div>

    </main>
