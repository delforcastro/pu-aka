<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>#DesafíoVictoria</title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">


		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">


		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>


		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		<script type="text/javascript" scr="https://cdnjs.cloudflare.com/ajax/libs/animateCSS/1.2.2/jquery.animatecss.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />
    <link rel="stylesheet" href="<?php echo base_url();?>/css/desafio/styles.css">
        <link rel="stylesheet" href="<?php echo base_url();?>/css/desafio/ticker.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link href="<?php echo base_url();?>/css/datatables.css" rel="stylesheet" />

    <!-- Requerimientos form-->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,100,300' rel='stylesheet' type='text/css'>

    <link href="https://fonts.googleapis.com/css?family=Raleway:100,400,700" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Oswald:200,400,700" rel="stylesheet">
    <script src="https://use.fontawesome.com/20ab91acc4.js"></script>
    <!-- Fin requerimientos form-->


</head>
<body>


	<!-- Carrusel -->
  <div class="container-fluid p-0">
		<!-- Barra de navegación-->
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" style="position: sticky">
	 <!-- Logo-Marca -->
	 <img class="navbar-brand" src="<?php echo base_url();?>/assets/logoDesafio.png" style="width:100px;" alt="Logo">
	 <!-- Icono 3barras -->
	 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
		 aria-label="Toggle navigation">
		 <span class="navbar-toggler-icon"></span>
	 </button>
	 <!-- Menu links  -->
	 <div class="collapse navbar-collapse" id="navbarNav">
		 <ul class="navbar-nav mr-auto">
			 <li class="nav-item">
				 <a class="nav-link text-success" href="#carousel">Inicio
					 <span class="sr-only">Inicio</span>
				 </a>
			 </li>
			 <li class="nav-item">
				 <a class="nav-link text-white" href="#acerca">Acerca de...</a>
			 </li>
			 <li class="nav-item">
				 <a class="nav-link text-white" href="#agenda">Agenda</a>
			 </li>
       <li class="nav-item">
        <a class="nav-link text-white" href="#historial">Historial de eventos</a>
      </li>
		 </ul>
		 <ul class="navbar-nav">

			 <li class="nav-item">
				 <a class="nav-link text-white" href="#contacto">Contáctanos</a>
			 </li>
		 </ul>
     <!--
           <form class="form-inline my-2 my-lg-0">
      			 <input class="form-control mr-sm-2" type="search" placeholder="Busca" aria-label="Search">
      			 <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar X</button>
      		 </form>
   -->
	 </div>
 </nav>


    <div class="news blue full-width">
    <span><i class="bi bi-star"></i>Novedades</span>
    <ul class="scrollLeft">
    <li><a href="#">Inscripción a #PescaDeLaMojarrita</a></li>
    <li><a href="#">Inscripción a #Carrera10k</a></li>
    <li><a href="#">Inscripción a #Carrera15k</a></li>
    <li><a href="#">Inscripción a #Carrera21k</a></li>

    </ul>
    </div>


 <a href="https://api.whatsapp.com/send?phone=+5493436449440" class="btn-wsp" target="_blank">
 	    <i class="bi bi-whatsapp"></i>
 	</a>






    <div id="carousel" class="carousel slide hero-slides" data-ride="carousel">


      <ol class="carousel-indicators">
        <li class="active" data-target="#carousel" data-slide-to="0"></li>
        <li data-target="#carousel" data-slide-to="1"></li>
        <li data-target="#carousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">

        <div class="carousel-item active boat">
          <div class="container h-100 d-md-block">
            <div class="row align-items-center h-100">
              <div class="col-12 col-md-9 col-lg-7 col-xl-6">
                <div class="caption animated fadeIn">
                  <h2 class="animated fadeInLeft"><b>Prepárate para la carrera más dura del litoral.</b></h2>
                  <p class="animated fadeInRight">Próximamente podrás inscribirte aquí.</p>
                  <a class="animated fadeInUp btn delicious-btn" href="#forminscripcionweb">Muy pronto</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="carousel-item sea">
          <div class="container h-100 d-md-block">
            <div class="row align-items-center h-100">
              <div class="col-12 col-md-9 col-lg-7 col-xl-6">
                <div class="caption animated fadeIn">
                  <h2 class="animated fadeInLeft">¡Pasala bien! <br>¡Vení a construir castillos de arena con nosotros!</h2>
                  <p class="animated fadeInRight">Próximamente podrás inscribirte aquí.</p>
                  <a class="animated fadeInUp btn delicious-btn" href="#">Muy pronto</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="carousel-item river">
          <div class="container h-100 d-md-block">
            <div class="row align-items-center h-100">
              <div class="col-12 col-md-9 col-lg-7 col-xl-6">
                <div class="caption animated fadeIn">
                  <h2 class="animated fadeInLeft">La carrera más divertida. ¡La Carrera del Carnaval!</h2>
                  <p class="animated fadeInRight">Próximamente podrás inscribirte aquí.</p>
                  <a class="animated fadeInUp btn delicious-btn" href="#">Muy pronto</a>
                </div>
              </div>
            </div>
          </div>
        </div>


      </div>

      <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>

    </div>






    <div class="container-fluid p-0" id="acerca">




      <h2>Contenido</h2>
    </div>




<!--
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
-->
<!-- COMENTADO 20211220

<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script type="text/javascript" scr="https://cdnjs.cloudflare.com/ajax/libs/animateCSS/1.2.2/jquery.animatecss.min.js"></script>
-->
