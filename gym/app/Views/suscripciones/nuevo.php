
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>
        </div>

        <form class="" action="<?php echo base_url(); ?>/suscripciones/insertar" method="post">
          <div class="form-group">
            <div class="row">

              <div class="col-12 col-sm-6">
                <label for="cliente">Cliente</label>
                <input list="clientes" id="cliente" name="cliente" class="form-control"/>
                <datalist id="clientes">
                  <option value="">Seleccionar cliente</option>
                  <?php
                    foreach ($clientes as $cliente) { ?>

                      <option value="<?php echo $cliente['id'] ?>"><?php echo $cliente['apellido'] . ", " . $cliente['nombre'] . " [DNI: " . $cliente['dni'] . "]"; ?></option>
                  <?php } ?>
                </datalist>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Servicio</label>
                <select class="form-control" name="servicio" id="servicio" required>
                  <option value="">Seleccionar servicio</option>
                  <?php
                    foreach ($servicios as $servicio) { ?>

                      <option value="<?php echo $servicio['id'] ?>"><?php echo $servicio['denominacion'] ?></option>
                  <?php } ?>
                </select>


              </div>




                <div class="col-12 col-sm-6">
                  <label for="">Mes</label>
                  <select class="form-control" name="mes" id="mes" required>
                    <option value="">Seleccione mes</option>
                      <option value="1">Enero</option>
                      <option value="2">Febrero</option>
                      <option value="3">Marzo</option>
                      <option value="4">Abril</option>
                      <option value="5">Mayo</option>
                      <option value="6">Junio</option>
                      <option value="7">Julio</option>
                      <option value="8">Agosto</option>
                      <option value="9">Septiembre</option>
                      <option value="10">Octubre</option>
                      <option value="11">Noviembre</option>
                      <option value="12">Diciembre</option>

                  </select>
                </div>
                <div class="col-12 col-sm-6">
                  <label for="">Año</label>
                  <select class="form-control" name="year" id="year" required>
                    <option value="">Seleccione año</option>
                      <option value="2022" selected>2022</option>
                      <option value="2023">2023</option>
                      <option value="2024">2024</option>

                  </select>
                </div>



            </div>
          </div>
            <a href="<?php echo base_url(); ?>/suscripciones" class="btn btn-primary">Volver</a>
            <button type="submit" name="button" class="btn btn-success">Guardar</button>

        </form>

    </main>
</div>
