<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>
        </div>

        <form class="" action="<?php echo base_url(); ?>/suscripciones/actualizar" method="post">
          <input type="hidden" name="id" id="id" value="<?php echo $suscripcion['id']; ?>">

          <div class="form-group">
            <div class="row">
              <div class="col-12 col-sm-6">
                <label for="">Cliente</label>
                <select class="form-control" name="cliente" id="cliente" required>
                  <option value="">Seleccionar cliente</option>
                  <?php
                    foreach ($clientes as $cliente) { ?>

                      <option value="<?php echo $cliente['id'] ?>" <?php if ($cliente['id']==$suscripcion['cliente']){ echo 'selected';} ?>><?php echo $cliente['apellido'] ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Servicio</label>
                <select class="form-control" name="servicio" id="servicio" required>
                  <option value="">Seleccionar servicio</option>
                  <?php
                    foreach ($servicios as $servicio) { ?>

                      <option value="<?php echo $servicio['id'] ?>" <?php if ($servicio['id']==$suscripcion['gym_servicio']){ echo 'selected';} ?>><?php echo $servicio['denominacion'] ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="col-12 col-sm-6">
                <label for="">Mes</label>
                <select class="form-control" name="mes" id="mes" required>
                  <option value="">Seleccione mes</option>
                    <option value="1" <?php if ($suscripcion['mes_alta']== 1){ echo 'selected';} ?>>Enero</option>
                    <option value="2" <?php if ($suscripcion['mes_alta']== 2){ echo 'selected';} ?>>Febrero</option>
                    <option value="3" <?php if ($suscripcion['mes_alta']== 3){ echo 'selected';} ?>>Marzo</option>
                    <option value="4" <?php if ($suscripcion['mes_alta']== 4){ echo 'selected';} ?>>Abril</option>
                    <option value="5" <?php if ($suscripcion['mes_alta']== 5){ echo 'selected';} ?>>Mayo</option>
                    <option value="6" <?php if ($suscripcion['mes_alta']== 6){ echo 'selected';} ?>>Junio</option>
                    <option value="7" <?php if ($suscripcion['mes_alta']== 7){ echo 'selected';} ?>>Julio</option>
                    <option value="8" <?php if ($suscripcion['mes_alta']== 8){ echo 'selected';} ?>>Agosto</option>
                    <option value="9" <?php if ($suscripcion['mes_alta']== 9){ echo 'selected';} ?>>Septiembre</option>
                    <option value="10" <?php if ($suscripcion['mes_alta']== 10){ echo 'selected';} ?>>Octubre</option>
                    <option value="11" <?php if ($suscripcion['mes_alta']== 11){ echo 'selected';} ?>>Noviembre</option>
                    <option value="12" <?php if ($suscripcion['mes_alta']== 12){ echo 'selected';} ?>>Diciembre</option>

                </select>
              </div>
              <div class="col-12 col-sm-6">
                <label for="">Año</label>
                <select class="form-control" name="year" id="year" required>
                  <option value="">Seleccione año</option>
                    <option value="2022" <?php if ($suscripcion['year_alta']=='2022'){ echo 'selected';} ?>>2022</option>
                    <option value="2023" <?php if ($suscripcion['year_alta']=='2023'){ echo 'selected';} ?>>2023</option>
                    <option value="2024" <?php if ($suscripcion['year_alta']=='2024'){ echo 'selected';} ?>>2024</option>

                </select>
              </div>

            </div>
          </div>
            <a href="<?php echo base_url(); ?>/suscripciones" class="btn btn-primary">Volver</a>
            <button type="submit" name="button" class="btn btn-success">Guardar</button>

        </form>

    </main>
</div>
