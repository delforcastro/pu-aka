
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4"><?php echo $titulo; ?></h1>

            <div>
              <p>
                <a href="<?php echo base_url();?>/suscripciones/nuevo" class= "btn btn-info">Agregar</a>
                <a href="<?php echo base_url();?>/suscripciones/eliminadas" class= "btn btn-warning">Eliminadas</a>

              </p>
            </div>

                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Cliente</th>
                                <th>Servicio</th>
                                <th>Acciones</th>

                            </tr>
                        </thead>

                        <tbody>
                          <?php foreach ($datos as $dato) { ?>
                              <tr>
                                <td><?php echo $dato['id'];?></td>
                                <td><?php foreach ($clientes as $cliente) {
                                  if ($cliente['id'] == $dato['cliente']){
                                    echo $cliente['apellido'] . ", " . $cliente['nombre'] . " (DNI " . $cliente['dni'] . ")";

                                  }
                                } ?></td>

                                <td><?php foreach ($servicios as $servicio) {
                                  if ($servicio['id'] == $dato['gym_servicio']){
                                    echo $servicio['denominacion'];

                                  }
                                } ?></td>


                                <td>
                                  <a href="<?php echo base_url();?>/suscripciones/editar/<?php echo $dato['id'];?>" class= "btn btn-primary"><i class="bi bi-pencil"></i></a>
                                  <a href="#" data-href="<?php echo base_url();?>/suscripciones/eliminar/<?php echo $dato['id'];?>" data-bs-toggle="modal" data-bs-target="#modalConfirma" class= "btn btn-danger"><i class="bi bi-trash"></i></a>
                                </td>

                              </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                </div>

    </main>
