<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\UsuariosModel;

class Usuarios extends BaseController{

  protected $usuarios, $session;

  public function __construct(){
    $this->usuarios = new UsuariosModel();
    $this->session = session();

  }

  public function index($activo = 1){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $usuarios = $this->usuarios->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Usuarios', 'datos' => $usuarios];

    echo view ('main/panel_header');
    echo view ('usuarios/usuarios', $data);
    echo view ('main/msgeliminar'); //trae modal de confirm de eliminado
    echo view ('main/panel_footer');
  }

  public function eliminadas($activo = 0){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $usuarios = $this->usuarios->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Usuarios eliminadas', 'datos' => $usuarios];

    echo view ('main/panel_header');
    echo view ('usuarios/eliminadas', $data);
    echo view ('main/msgrestaurar'); //trae modal de confirm de restaurado
    echo view ('main/panel_footer');
  }

  public function nuevo(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $data = ['titulo'=>'Agregar usuario'];
    echo view ('main/panel_header');
    echo view ('usuarios/nuevo', $data);
    echo view ('main/panel_footer');
  }

  public function insertar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $hash = password_hash($this->request->getPost('pss'), PASSWORD_DEFAULT);

    $this->usuarios->save(['user' => $this->request->getPost('user'),
    'pss' => $hash,
    'nombre' => $this->request->getPost('nombre')]);

    return redirect()->to(base_url().'/usuarios');

  }

  public function editar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $usuarios = $this->usuarios->where('id', $id)->first();

    $data = ['titulo'=>'Editar usuario', 'datos' => $usuarios];
    echo view ('main/panel_header');
    echo view ('usuarios/editar', $data);
    echo view ('main/panel_footer');
  }

  public function actualizar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->usuarios->update($this->request->getPost('id'),['user' => $this->request->getPost('user'),
    'pss' => $this->request->getPost('pss'),
    'nombre' => $this->request->getPost('nombre')]);
    return redirect()->to(base_url().'/usuarios');

  }

  public function eliminar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->usuarios->update($id, ['activo' => 0]);
    return redirect()->to(base_url().'/usuarios');

  }

  public function restaurar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->usuarios->update($id, ['activo' => 1]);
    return redirect()->to(base_url().'/usuarios');

  }

  public function cambiapassword(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    //$session = session();
    //$usuario = $this->usuarios->where('id', $session->id_usuario)->first();
    $usuario = $this->usuarios->where('id', $this->session->id_usuario)->first();
    $data = ['titulo'=>'Cambiar contraseña', 'usuario' =>$usuario];

    echo view ('main/panel_header');
    echo view ('usuarios/cambiapassword', $data);
    echo view ('main/panel_footer');
  }


  public function actualizar_password(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    //$session = session();
    //$idUsuario = $session-> id_usuario;
    $idUsuario = $this->session-> id_usuario;
    $hash = password_hash($this->request->getPost('pss'), PASSWORD_DEFAULT);

    $this->usuarios->update($idUsuario, ['pss' => $hash]);

    return redirect()->to(base_url().'/panel');
  }


}
