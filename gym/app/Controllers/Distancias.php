<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\DistanciasModel;

class Distancias extends BaseController{

  protected $distancias, $session;

  public function __construct(){
    $this->distancias = new DistanciasModel();
    $this->session = session();

  }

  public function index($activo = 1){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $distancias = $this->distancias->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Distancias', 'datos' => $distancias];

    echo view ('main/panel_header');
    echo view ('distancias/distancias', $data);
    echo view ('main/msgeliminar'); //trae modal de confirm de eliminado
    echo view ('main/panel_footer');
  }

  public function eliminadas($activo = 0){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $distancias = $this->distancias->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Distancias eliminados', 'datos' => $distancias];

    echo view ('main/panel_header');
    echo view ('distancias/eliminadas', $data);
    echo view ('main/msgrestaurar'); //trae modal de confirm de restaurado
    echo view ('main/panel_footer');
  }

  public function nuevo(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $data = ['titulo'=>'Agregar distancia'];
    echo view ('main/panel_header');
    echo view ('distancias/nuevo', $data);
    echo view ('main/panel_footer');
  }

  public function insertar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->distancias->save(['denominacion' => $this->request->getPost('denominacion')]);
    return redirect()->to(base_url().'/distancias');

  }

  public function editar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $distancia = $this->distancias->where('id', $id)->first();

    $data = ['titulo'=>'Editar distancia', 'datos' => $distancia];
    echo view ('main/panel_header');
    echo view ('distancias/editar', $data);
    echo view ('main/panel_footer');
  }

  public function actualizar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->distancias->update($this->request->getPost('id'),['denominacion' => $this->request->getPost('denominacion')]);
    return redirect()->to(base_url().'/distancias');

  }

  public function eliminar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->distancias->update($id, ['activo' => 0]);
    return redirect()->to(base_url().'/distancias');

  }

  public function restaurar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->distancias->update($id, ['activo' => 1]);
    return redirect()->to(base_url().'/distancias');

  }
}
