<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ProfesionesModel;

class Profesiones extends BaseController{

  protected $profesiones, $session;

  public function __construct(){
    $this->profesiones = new ProfesionesModel();
    $this->session = session();


  }

  public function index($activo = 1){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $profesiones = $this->profesiones->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Profesiones', 'datos' => $profesiones];

    echo view ('main/panel_header');
    echo view ('profesiones/profesiones', $data);
    echo view ('main/msgeliminar'); //trae modal de confirm de eliminado
    echo view ('main/panel_footer');
  }

  public function eliminadas($activo = 0){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $profesiones = $this->profesiones->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Profesiones eliminadas', 'datos' => $profesiones];

    echo view ('main/panel_header');
    echo view ('profesiones/eliminadas', $data);
    echo view ('main/msgrestaurar'); //trae modal de confirm de restaurado
    echo view ('main/panel_footer');
  }

  public function nuevo(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $data = ['titulo'=>'Agregar profesión'];
    echo view ('main/panel_header');
    echo view ('profesiones/nuevo', $data);
    echo view ('main/panel_footer');
  }

  public function insertar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    if ($this->request->getMethod() =="post" && $this->validate(['denominacion' => 'required'])){
      $this->profesiones->save(['denominacion' => $this->request->getPost('denominacion'),
      'mes' => $this->request->getPost('mes'),
      'dia' => $this->request->getPost('dia')]);

      return redirect()->to(base_url().'/profesiones');
    }else{
      $data = ['titulo'=>'Agregar profesión', 'validation'=>$this->validator];
      echo view ('main/panel_header');
      echo view ('profesiones/nuevo', $data);
      echo view ('main/panel_footer');
    }


  }

  public function editar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }
    $profesion = $this->profesiones->where('id', $id)->first();

    $data = ['titulo'=>'Editar profesión', 'datos' => $profesion];
    echo view ('main/panel_header');
    echo view ('profesiones/editar', $data);
    echo view ('main/panel_footer');
  }

  public function actualizar(){
    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->profesiones->update($this->request->getPost('id'),['denominacion' => $this->request->getPost('denominacion'),
    'mes' => $this->request->getPost('mes'),
    'dia' => $this->request->getPost('dia')]);

    return redirect()->to(base_url().'/profesiones');

  }

  public function eliminar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->profesiones->update($id, ['activo' => 0]);
    return redirect()->to(base_url().'/profesiones');

  }

  public function restaurar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }


    $this->profesiones->update($id, ['activo' => 1]);
    return redirect()->to(base_url().'/profesiones');

  }
}
