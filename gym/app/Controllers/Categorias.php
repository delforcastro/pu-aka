<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\CategoriasModel;

class Categorias extends BaseController{

  protected $categorias, $session;

  public function __construct(){
    $this->categorias = new CategoriasModel();
    $this->session = session();

  }

  public function index($activo = 1){
    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $categorias = $this->categorias->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Categorías', 'datos' => $categorias];

    echo view ('main/panel_header');
    echo view ('categorias/categorias', $data);
    echo view ('main/msgeliminar'); //trae modal de confirm de eliminado
    echo view ('main/panel_footer');
  }

  public function eliminadas($activo = 0){
    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $categorias = $this->categorias->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Categorías eliminadas', 'datos' => $categorias];

    echo view ('main/panel_header');
    echo view ('categorias/eliminadas', $data);
    echo view ('main/msgrestaurar'); //trae modal de confirm de restaurado
    echo view ('main/panel_footer');
  }

  public function nuevo(){
    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $data = ['titulo'=>'Agregar categoría'];
    echo view ('main/panel_header');
    echo view ('categorias/nuevo', $data);
    echo view ('main/panel_footer');
  }

  public function insertar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->categorias->save(['denominacion' => $this->request->getPost('denominacion')]);
    return redirect()->to(base_url().'/categorias');

  }

  public function editar($id){
    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $categoria = $this->categorias->where('id', $id)->first();

    $data = ['titulo'=>'Editar categoría', 'datos' => $categoria];
    echo view ('main/panel_header');
    echo view ('categorias/editar', $data);
    echo view ('main/panel_footer');
  }

  public function actualizar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->categorias->update($this->request->getPost('id'),['denominacion' => $this->request->getPost('denominacion')]);
    return redirect()->to(base_url().'/categorias');

  }

  public function eliminar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->categorias->update($id, ['activo' => 0]);
    return redirect()->to(base_url().'/categorias');

  }

  public function restaurar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }
    
    $this->categorias->update($id, ['activo' => 1]);
    return redirect()->to(base_url().'/categorias');

  }
}
