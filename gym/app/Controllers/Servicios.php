<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ServiciosModel;

class Servicios extends BaseController{

  protected $servicios, $session;

  public function __construct(){
    $this->servicios = new ServiciosModel();
    $this->session = session();


  }

  public function index($activo = 1){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $servicios = $this->servicios->where('activo', $activo)->findAll();
    $admin = $this->session->admin;

    $data = ['titulo'=>'Servicios', 'datos' => $servicios, 'admin'=>$admin];

    echo view ('main/panel_header');
    echo view ('servicios/servicios', $data);
    echo view ('main/msgeliminar'); //trae modal de confirm de eliminado
    echo view ('main/panel_footer');
  }

  public function eliminadas($activo = 0){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $servicios = $this->servicios->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Servicios eliminados', 'datos' => $servicios];

    echo view ('main/panel_header');
    echo view ('servicios/eliminadas', $data);
    echo view ('main/msgrestaurar'); //trae modal de confirm de restaurado
    echo view ('main/panel_footer');
  }

  public function nuevo(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $data = ['titulo'=>'Agregar servicio'];
    echo view ('main/panel_header');
    echo view ('servicios/nuevo', $data);
    echo view ('main/panel_footer');
  }

  public function insertar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    if ($this->request->getMethod() =="post" && $this->validate(['denominacion' => 'required'])){
      $this->servicios->save(['denominacion' => $this->request->getPost('denominacion')]);
      return redirect()->to(base_url().'/servicios');
    }else{
      $data = ['titulo'=>'Agregar servicio', 'validation'=>$this->validator];
      echo view ('main/panel_header');
      echo view ('servicios/nuevo', $data);
      echo view ('main/panel_footer');
    }


  }

  public function editar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }
    $servicio = $this->servicios->where('id', $id)->first();

    $data = ['titulo'=>'Editar servicio', 'datos' => $servicio];
    echo view ('main/panel_header');
    echo view ('servicios/editar', $data);
    echo view ('main/panel_footer');
  }

  public function actualizar(){
    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->servicios->update($this->request->getPost('id'),['denominacion' => $this->request->getPost('denominacion')]);
    return redirect()->to(base_url().'/servicios');

  }

  public function eliminar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->servicios->update($id, ['activo' => 0]);
    return redirect()->to(base_url().'/servicios');

  }

  public function restaurar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }


    $this->servicios->update($id, ['activo' => 1]);
    return redirect()->to(base_url().'/servicios');

  }
}
