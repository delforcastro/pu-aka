<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\MonedasModel;

class Monedas extends BaseController{

  protected $monedas, $session;

  public function __construct(){
    $this->monedas = new MonedasModel();
    $this->session = session();

  }

  public function index($activo = 1){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $monedas = $this->monedas->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Monedas', 'datos' => $monedas];

    echo view ('main/panel_header');
    echo view ('monedas/monedas', $data);
    echo view ('main/msgeliminar'); //trae modal de confirm de eliminado
    echo view ('main/panel_footer');
  }

  public function eliminadas($activo = 0){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $monedas = $this->monedas->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Monedas eliminadas', 'datos' => $monedas];

    echo view ('main/panel_header');
    echo view ('monedas/eliminadas', $data);
    echo view ('main/msgrestaurar'); //trae modal de confirm de restaurado
    echo view ('main/panel_footer');
  }

  public function nuevo(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $data = ['titulo'=>'Agregar moneda'];
    echo view ('main/panel_header');
    echo view ('monedas/nuevo', $data);
    echo view ('main/panel_footer');
  }

  public function insertar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->monedas->save(['denominacion' => $this->request->getPost('denominacion'), 'simbolo' => $this->request->getPost('simbolo')]);
    return redirect()->to(base_url().'/monedas');

  }

  public function editar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $moneda = $this->monedas->where('id', $id)->first();

    $data = ['titulo'=>'Editar moneda', 'datos' => $moneda];
    echo view ('main/panel_header');
    echo view ('monedas/editar', $data);
    echo view ('main/panel_footer');
  }

  public function actualizar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->monedas->update($this->request->getPost('id'),['denominacion' => $this->request->getPost('denominacion'), 'simbolo' => $this->request->getPost('simbolo')]);
    return redirect()->to(base_url().'/monedas');

  }

  public function eliminar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->monedas->update($id, ['activo' => 0]);
    return redirect()->to(base_url().'/monedas');

  }

  public function restaurar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }
    
    $this->monedas->update($id, ['activo' => 1]);
    return redirect()->to(base_url().'/monedas');

  }
}
