<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ProvinciasModel;

class Provincias extends BaseController{

  protected $provincias, $session;

  public function __construct(){
    $this->provincias = new ProvinciasModel();
    $this->session = session();

  }

  public function index($activo = 1){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $provincias = $this->provincias->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Provincias', 'datos' => $provincias];

    echo view ('main/panel_header');
    echo view ('provincias/provincias', $data);
    echo view ('main/msgeliminar'); //trae modal de confirm de eliminado
    echo view ('main/panel_footer');
  }

  public function eliminadas($activo = 0){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $provincias = $this->provincias->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Provincias eliminadas', 'datos' => $provincias];

    echo view ('main/panel_header');
    echo view ('provincias/eliminadas', $data);
    echo view ('main/msgrestaurar'); //trae modal de confirm de restaurado
    echo view ('main/panel_footer');
  }

  public function nuevo(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $data = ['titulo'=>'Agregar provincia'];
    echo view ('main/panel_header');
    echo view ('provincias/nuevo', $data);
    echo view ('main/panel_footer');
  }

  public function insertar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->provincias->save(['denominacion' => $this->request->getPost('denominacion'), 'simbolo' => $this->request->getPost('simbolo')]);
    return redirect()->to(base_url().'/provincias');

  }

  public function editar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $provincia = $this->provincias->where('id', $id)->first();

    $data = ['titulo'=>'Editar provincia', 'datos' => $provincia];
    echo view ('main/panel_header');
    echo view ('provincias/editar', $data);
    echo view ('main/panel_footer');
  }

  public function actualizar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->provincias->update($this->request->getPost('id'),['denominacion' => $this->request->getPost('denominacion'), 'simbolo' => $this->request->getPost('simbolo')]);
    return redirect()->to(base_url().'/provincias');

  }

  public function eliminar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->provincias->update($id, ['activo' => 0]);
    return redirect()->to(base_url().'/provincias');

  }

  public function restaurar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->provincias->update($id, ['activo' => 1]);
    return redirect()->to(base_url().'/provincias');

  }
}
