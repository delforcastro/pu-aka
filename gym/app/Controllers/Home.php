<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\InscripcionesModel;
use App\Models\EventosModel;
use App\Models\DistanciasModel;
use App\Models\CategoriasModel;
use App\Models\ProvinciasModel;
use App\Models\TallesModel;

class Home extends BaseController
{


  public function __construct(){
    $this->inscripciones = new InscripcionesModel();
    $this->eventos = new EventosModel();
    $this->distancias = new DistanciasModel();
    $this->categorias = new CategoriasModel();
    $this->provincias = new ProvinciasModel();
    $this->talles = new TallesModel();

  }

    public function index()
    {

      #empiezo

      $eventos = $this->eventos->where('activo', 1)->findAll();
      $distancias = $this->distancias->where('activo', 1)->findAll();
      $categorias = $this->categorias->where('activo', 1)->findAll();
      $provincias = $this->provincias->where('activo', 1)->findAll();
      $talles = $this->talles->where('activo', 1)->findAll();
      $inscripciones = $this->inscripciones->where('activo', 1)->findAll();

      $data = ['titulo'=>'Inscripción', 'eventos' => $eventos,
              'distancias' => $distancias, 'categorias' => $categorias,
              'provincias' => $provincias, 'talles' => $talles, 'datos' => $inscripciones];

      #termino
      echo view('home');
      echo view ('inscripciones/inscripcionweb', $data);
      echo view ('contacto');
      echo view ('main/msgconfirmarweb');
      echo view ('footer');

    }

    function enviarEmail()
    {
      $email = \Config\Services::email();

      $name = $this->request->getPost('name');
      $from_email = $this->request->getPost('email');
      $subject = "Contacto web | Teléfono: " . $this->request->getPost('phone');
      $message = "<b>Nombre:</b><br>" . $name . "<br><b>Correo electrónico:</b><br><a href=\"mailto:" . $from_email  . "\">" . $from_email  . "</a><br><b>Mensaje:</b><br>" . $this->request->getPost('message');



      $email->setFrom('info@desafiovictoria.com.ar', 'Contacto Web');
      $email->setTo('info@desafiovictoria.com.ar');
      //$email->setCC('another@another-example.com');
      //$email->setBCC('them@their-example.com');

      $email->setSubject($subject);
      $email->setMessage($message);

      $email->send();
      return redirect()->back();

}
}
