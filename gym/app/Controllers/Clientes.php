<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ClientesModel;
use App\Models\SuscripcionesModel;
use App\Models\ServiciosModel;
use App\Models\ProfesionesModel;
use App\Models\TarifasModel;
use App\Models\PagosModel;

class Clientes extends BaseController{

  protected $clientes, $session;

  public function __construct(){
    $this->clientes = new ClientesModel();
    $this->suscripciones = new SuscripcionesModel();
    $this->servicios = new ServiciosModel();
    $this->profesiones = new ProfesionesModel();
    $this->tarifas = new TarifasModel();
    $this->pagos = new PagosModel();
    $this->session = session();


  }

  public function index($activo = 1){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $clientes = $this->clientes->where('activo', $activo)
                ->orderBy('apellido', 'asc')
                ->findAll();

    $admin = $this->session->admin;


    $data = ['titulo'=>'Clientes', 'datos' => $clientes, 'admin'=>$admin];

    echo view ('main/panel_header',);
    echo view ('clientes/clientes', $data);
    echo view ('main/msgeliminar'); //trae modal de confirm de eliminado
    echo view ('main/panel_footer');
  }

  public function eliminadas($activo = 0){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $clientes = $this->clientes->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Clientes eliminados', 'datos' => $clientes];

    echo view ('main/panel_header');
    echo view ('clientes/eliminadas', $data);
    echo view ('main/msgrestaurar'); //trae modal de confirm de restaurado
    echo view ('main/panel_footer');
  }

  public function nuevo(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }
    $profesiones = $this->profesiones->where('activo', 1)
                ->orderBy('denominacion', 'asc')
                ->findAll();

    $data = ['titulo'=>'Agregar cliente', 'profesiones' =>$profesiones];
    echo view ('main/panel_header');
    echo view ('clientes/nuevo', $data);
    echo view ('main/panel_footer');
  }

  public function insertar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    if ($this->request->getMethod() =="post" && $this->validate(['apellido' => 'required',
    'nombre' => 'required'])){
      $this->clientes->save(['apellido' => $this->request->getPost('apellido'),
      'nombre' => $this->request->getPost('nombre'),
      'dni' => $this->request->getPost('dni'),
      'grupo_sang' => $this->request->getPost('grupo_sang'),
      'instagram' => $this->request->getPost('instagram'),
      'telefono' => $this->request->getPost('telefono'),
      'email' => $this->request->getPost('email'),
      'fecha_nacimiento' => $this->request->getPost('fecha_nacimiento'),
      'gym_profesion' => $this->request->getPost('profesion'),
      'obs' => $this->request->getPost('obs')
      ]);
      return redirect()->to(base_url().'/clientes');
    }else{
      $data = ['titulo'=>'Agregar cliente', 'validation'=>$this->validator];
      echo view ('main/panel_header');
      echo view ('clientes/nuevo', $data);
      echo view ('main/panel_footer');
    }


  }

  public function editar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }
    $cliente = $this->clientes->where('id', $id)->first();
    $profesiones = $this->profesiones->where('activo', 1)
                ->orderBy('denominacion', 'asc')
                ->findAll();

    $data = ['titulo'=>'Editar cliente', 'datos' => $cliente, 'profesiones'=>$profesiones];
    echo view ('main/panel_header');
    echo view ('clientes/editar', $data);
    echo view ('main/panel_footer');
  }

  public function actualizar(){
    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->clientes->update($this->request->getPost('id'),['apellido' => $this->request->getPost('apellido'),
      'nombre' => $this->request->getPost('nombre'),
      'dni' => $this->request->getPost('dni'),
      'grupo_sang' => $this->request->getPost('grupo_sang'),
      'instagram' => $this->request->getPost('instagram'),
      'telefono' => $this->request->getPost('telefono'),
      'email' => $this->request->getPost('email'),
      'fecha_nacimiento' => $this->request->getPost('fecha_nacimiento'),
      'gym_profesion' => $this->request->getPost('profesion'),
      'obs' => $this->request->getPost('obs')
      ]);
    return redirect()->to(base_url().'/clientes');

  }

  public function eliminar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->clientes->update($id, ['activo' => 0]);
    return redirect()->to(base_url().'/clientes');

  }

  public function restaurar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }


    $this->clientes->update($id, ['activo' => 1]);
    return redirect()->to(base_url().'/clientes');

  }

  public function status($id){
    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $cliente = $this->clientes->where('id', $id)
                ->first();


    $servicios = $this->servicios->where('activo',1)
                ->findAll();

    $suscripciones = $this->suscripciones->where('activo', 1)
                ->where('cliente', $id)
                ->findAll();

    $pagos = $this->pagos->where('activo', 1)
                  ->where('cliente', $id)
                  ->findAll();

    $tarifas = $this->tarifas->where('activo', 1)
              ->findAll();


    $db = \Config\Database::connect();

    $builder = $db->table('gym_pagos p');
    $builder->select('p.id, p.suscripcion, p.monto, c.apellido, c.nombre, p.activo, c.id');
    $builder->join('suscripciones s', 'p.suscripcion=s.id');
    $builder->join('clientes c', 's.cliente=c.id');
    $builder->where('c.id', $id);
    $query = $builder->get();
    $pagos = $query->getResultArray();



    //con el siguiente builder obtengo las liquidaciones
    $tbuilder = $db->table('gym_tarifas t');
    $tbuilder -> select ('t.id as `tid`, t.mes, t.year, t.servicio, t.fecha_vencimiento, t.monto, t.activo, s.denominacion, sus.id as `susid`');
    $tbuilder -> join('gym_servicios s', 't.servicio=s.id');
    $tbuilder -> join('suscripciones sus', 's.id=sus.gym_servicio');
    $tbuilder -> join('clientes c', 'sus.cliente=c.id');
    $tbuilder -> where ('c.id', $id);
    $tbuilder -> where('sus.activo', 1);
    $tbuilder -> where('t.activo', 1);
    $tbuilder -> where ('t.periodo>=sus.fecha_suscripcion'); //qudé acá viendo cómo resolver el tema de las fechas
    $tbuilder -> orderBy('t.periodo', 'DESC');
    //$liqstr = $tbuilder -> getCompiledSelect();
    $tafs = $tbuilder -> get();
    $liquidaciones = $tafs->getResultArray();


    $data = ['titulo'=>'Estado del cliente', 'cliente' => $cliente, 'servicios'=> $servicios, 'suscripciones'=>$suscripciones, 'tarifas'=>$tarifas, 'pagos'=>$pagos, 'liquidaciones'=>$liquidaciones, 'cadena' => $liqstr];
    echo $sql;
    echo view ('main/panel_header');
    echo view ('clientes/status', $data);
    echo view ('main/msgeliminar'); //trae modal de confirm de eliminado
    echo view ('main/panel_footer');
  }
}
