<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\CiudadesModel;

class Ciudades extends BaseController{

  protected $ciudades, $session;

  public function __construct(){
    $this->ciudades = new CiudadesModel();
    $this->session = session();


  }

  public function index($activo = 1){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $ciudades = $this->ciudades->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Ciudades', 'datos' => $ciudades];

    echo view ('main/panel_header');
    echo view ('ciudades/ciudades', $data);
    echo view ('main/msgeliminar'); //trae modal de confirm de eliminado
    echo view ('main/panel_footer');
  }

  public function eliminadas($activo = 0){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $ciudades = $this->ciudades->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Ciudades eliminadas', 'datos' => $ciudades];

    echo view ('main/panel_header');
    echo view ('ciudades/eliminadas', $data);
    echo view ('main/msgrestaurar'); //trae modal de confirm de restaurado
    echo view ('main/panel_footer');
  }

  public function nuevo(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $data = ['titulo'=>'Agregar ciudad'];
    echo view ('main/panel_header');
    echo view ('ciudades/nuevo', $data);
    echo view ('main/panel_footer');
  }

  public function insertar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    if ($this->request->getMethod() =="post" && $this->validate(['denominacion' => 'required'])){
      $this->ciudades->save(['denominacion' => $this->request->getPost('denominacion')]);
      return redirect()->to(base_url().'/ciudades');
    }else{
      $data = ['titulo'=>'Agregar ciudad', 'validation'=>$this->validator];
      echo view ('main/panel_header');
      echo view ('ciudades/nuevo', $data);
      echo view ('main/panel_footer');
    }


  }

  public function editar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }
    $ciudad = $this->ciudades->where('id', $id)->first();

    $data = ['titulo'=>'Editar ciudad', 'datos' => $ciudad];
    echo view ('main/panel_header');
    echo view ('ciudades/editar', $data);
    echo view ('main/panel_footer');
  }

  public function actualizar(){
    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->ciudades->update($this->request->getPost('id'),['denominacion' => $this->request->getPost('denominacion')]);
    return redirect()->to(base_url().'/ciudades');

  }

  public function eliminar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->ciudades->update($id, ['activo' => 0]);
    return redirect()->to(base_url().'/ciudades');

  }

  public function restaurar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    
    $this->ciudades->update($id, ['activo' => 1]);
    return redirect()->to(base_url().'/ciudades');

  }
}
