<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\TallesModel;

class Talles extends BaseController{

  protected $talles, $session;

  public function __construct(){
    $this->talles = new TallesModel();
    $this->session = session();


  }

  public function index($activo = 1){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $talles = $this->talles->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Talles', 'datos' => $talles];

    echo view ('main/panel_header');
    echo view ('talles/talles', $data);
    echo view ('main/msgeliminar'); //trae modal de confirm de eliminado
    echo view ('main/panel_footer');
  }

  public function eliminadas($activo = 0){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $talles = $this->talles->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Talles eliminadas', 'datos' => $talles];

    echo view ('main/panel_header');
    echo view ('talles/eliminadas', $data);
    echo view ('main/msgrestaurar'); //trae modal de confirm de restaurado
    echo view ('main/panel_footer');
  }

  public function nuevo(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $data = ['titulo'=>'Agregar talle'];
    echo view ('main/panel_header');
    echo view ('talles/nuevo', $data);
    echo view ('main/panel_footer');
  }

  public function insertar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    if ($this->request->getMethod() =="post" && $this->validate(['denominacion' => 'required'])){
      $this->talles->save(['denominacion' => $this->request->getPost('denominacion')]);
      return redirect()->to(base_url().'/talles');
    }else{
      $data = ['titulo'=>'Agregar talle', 'validation'=>$this->validator];
      echo view ('main/panel_header');
      echo view ('talles/nuevo', $data);
      echo view ('main/panel_footer');
    }


  }

  public function editar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }
    $talle = $this->talles->where('id', $id)->first();

    $data = ['titulo'=>'Editar talle', 'datos' => $talle];
    echo view ('main/panel_header');
    echo view ('talles/editar', $data);
    echo view ('main/panel_footer');
  }

  public function actualizar(){
    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->talles->update($this->request->getPost('id'),['denominacion' => $this->request->getPost('denominacion')]);
    return redirect()->to(base_url().'/talles');

  }

  public function eliminar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->talles->update($id, ['activo' => 0]);
    return redirect()->to(base_url().'/talles');

  }

  public function restaurar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }


    $this->talles->update($id, ['activo' => 1]);
    return redirect()->to(base_url().'/talles');

  }
}
