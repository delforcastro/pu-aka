<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ClientesModel;

class Panel extends BaseController
{
  protected $clientes, $session, $tclientes;

  public function __construct()
  {
    $this->clientes = new ClientesModel();
    $this->session = session();
  }
  public function index($activo = 1){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $tclientes = $this->clientes->where('activo', $activo)
                ->countAllResults();
    $admin = $this->session->admin;

    $tcumple = $this->clientes->where('MONTH(fecha_nacimiento)', date('m'))
                ->countAllResults();


    $data = ['tclientes' => $tclientes, 'tcumple' => $tcumple];
    $profile = ['admin' => $admin];
    echo view ('main/panel_header', $profile);
    echo view ('contenido', $data);
    echo view ('main/panel_footer');
  }



}
