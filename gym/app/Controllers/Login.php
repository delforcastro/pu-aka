<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\UsuariosModel;


class Login extends BaseController
{

  protected $usuarios;

  public function __construct(){
    $this->usuarios = new UsuariosModel();

  }

    public function index()
    {
      helper('form');
      echo view('main/header');
      echo view ('admin');
      echo view ('main/footer');
    }

    public function login_validation(){

        $user = $this->request->getPost('user');
        $pass = $this->request->getPost('pass');
        $datosUsuario = $this->usuarios->where('user', $user)->first();


        if ($datosUsuario != null){
          if (password_verify($pass, $datosUsuario['pss'])){
            $data_sesion = [
              'id_usuario' => $datosUsuario['id'],
              'user' => $datosUsuario['user'],
              'nombre' => $datosUsuario['nombre'],
              'admin'=>$datosUsuario['admin']
            ];
            $sesion = session();
            $sesion->set($data_sesion);

            return redirect()->to(base_url() . '/panel');


          }else{
            echo "Las credenciales no son correctas.";
          }
        }else{
          echo "Las credenciales no son correctas.";
        }

    }

    public function logout(){
      $sesion = session();
      $sesion->destroy();
      return redirect()->to(base_url() . '/login');
    }
  }
