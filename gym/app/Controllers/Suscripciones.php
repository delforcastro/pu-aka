<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\SuscripcionesModel;
use App\Models\ClientesModel;
use App\Models\ServiciosModel;


class Suscripciones extends BaseController{

  protected $suscripciones, $session;

  public function __construct(){
    $this->suscripciones = new SuscripcionesModel();
    $this->clientes = new ClientesModel();
    $this->servicios = new ServiciosModel();

    $this->session = session();

  }

  public function index($activo = 1){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $clientes = $this->clientes->where('activo', 1)
                ->orderBy('apellido', 'asc')
                ->findAll();
    $servicios = $this->servicios->where('activo', 1)->findAll();


    $suscripciones = $this->suscripciones->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Suscripciones', 'clientes' => $clientes,
            'servicios' => $servicios, 'datos' => $suscripciones];

    echo view ('main/panel_header');
    echo view ('suscripciones/suscripciones', $data);
    echo view ('main/msgeliminar'); //trae modal de confirm de eliminado
    echo view ('main/panel_footer');
  }

  public function eliminadas($activo = 0){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }
    $clientes = $this->clientes->where('activo', 1)->findAll();
    $servicios = $this->servicios->where('activo', 1)->findAll();

    $suscripciones = $this->suscripciones->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Suscripciones anuladas', 'clientes' => $clientes,
            'servicios' => $servicios, 'datos' => $suscripciones];

    echo view ('main/panel_header');
    echo view ('suscripciones/eliminadas', $data);
    echo view ('main/msgrestaurar'); //trae modal de confirm de restaurado
    echo view ('main/panel_footer');
  }

  public function nuevo(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $clientes = $this->clientes->where('activo', 1)->findAll();
    $servicios = $this->servicios->where('activo', 1)->findAll();


    $data = ['titulo'=>'Agregar suscripción', 'clientes' => $clientes,
            'servicios' => $servicios];
    echo view ('main/panel_header');
    echo view ('suscripciones/nuevo', $data);
    echo view ('main/panel_footer');
  }

  public function insertar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->suscripciones->save(['cliente' => $this->request->getPost('cliente'),
    'gym_servicio' => $this->request->getPost('servicio'),
    'mes_alta' => $this->request->getPost('mes'),
    'year_alta' => $this->request->getPost('year'),
    'fecha_suscripcion' => date($this->request->getPost('year') . "-" . $this->request->getPost('mes') . "-01")
    ]);
    return redirect()->to(base_url().'/suscripciones');

  }



  public function editar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $clientes = $this->clientes->where('activo', 1)->findAll();
    $servicios = $this->servicios->where('activo', 1)->findAll();


    $suscripcion = $this->suscripciones->where('id', $id)->first();

    $data = ['titulo'=>'Editar suscripción', 'clientes' => $clientes,
            'servicios' => $servicios, 'suscripcion' => $suscripcion];


    echo view ('main/panel_header');
    echo view ('suscripciones/editar', $data);
    echo view ('main/panel_footer');
  }

  public function actualizar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->suscripciones->update($this->request->getPost('id'),['cliente' => $this->request->getPost('cliente'),
    'gym_servicio' => $this->request->getPost('servicio'),
    'mes_alta' => $this->request->getPost('mes'),
    'year_alta' => $this->request->getPost('year'),
    ]);
    return redirect()->to(base_url().'/suscripciones');

  }

  public function eliminar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->suscripciones->update($id, ['activo' => 0]);
    return redirect()->to(base_url().'/suscripciones');

  }



  public function restaurar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->suscripciones->update($id, ['activo' => 1]);
    return redirect()->to(base_url().'/suscripciones');

  }
}
