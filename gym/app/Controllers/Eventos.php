<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\EventosModel;

class Eventos extends BaseController{

  protected $eventos, $session;

  public function __construct(){
    $this->eventos = new EventosModel();
    $this->session = session();

  }

  public function index($activo = 1){
    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $eventos = $this->eventos->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Eventos', 'datos' => $eventos];

    echo view ('main/panel_header');
    echo view ('eventos/eventos', $data);
    echo view ('main/msgeliminar'); //trae modal de confirm de eliminado
    echo view ('main/panel_footer');
  }

  public function eliminadas($activo = 0){
    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $eventos = $this->eventos->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Eventos eliminadas', 'datos' => $eventos];

    echo view ('main/panel_header');
    echo view ('eventos/eliminadas', $data);
    echo view ('main/msgrestaurar'); //trae modal de confirm de restaurado
    echo view ('main/panel_footer');
  }

  public function nuevo(){
    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $data = ['titulo'=>'Agregar categoría'];
    echo view ('main/panel_header');
    echo view ('eventos/nuevo', $data);
    echo view ('main/panel_footer');
  }

  public function insertar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->eventos->save(['denominacion' => $this->request->getPost('denominacion'),
    'ciudad' => $this->request->getPost('ciudad'),
    'fecha' => $this->request->getPost('fecha')
    ]);
    return redirect()->to(base_url().'/eventos');

  }

  public function editar($id){
    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $evento = $this->eventos->where('id', $id)->first();

    $data = ['titulo'=>'Editar evento', 'datos' => $evento];
    echo view ('main/panel_header');
    echo view ('eventos/editar', $data);
    echo view ('main/panel_footer');
  }

  public function actualizar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->eventos->update($this->request->getPost('id'),['denominacion' => $this->request->getPost('denominacion'),
    'ciudad' => $this->request->getPost('ciudad'),
    'fecha' => $this->request->getPost('fecha')
    ]);
    return redirect()->to(base_url().'/eventos');

  }

  public function eliminar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->eventos->update($id, ['activo' => 0]);
    return redirect()->to(base_url().'/eventos');

  }

  public function restaurar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->eventos->update($id, ['activo' => 1]);
    return redirect()->to(base_url().'/eventos');

  }
}
