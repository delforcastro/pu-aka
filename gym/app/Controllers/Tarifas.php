<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ServiciosModel;
use App\Models\TarifasModel;


class Tarifas extends BaseController{

  protected $suscripciones, $session;

  public function __construct(){
    $this->tarifas = new TarifasModel();
    $this->servicios = new ServiciosModel();

    $this->session = session();

  }

  public function index($activo = 1){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $servicios = $this->servicios->where('activo', 1)->findAll();


    $tarifas = $this->tarifas->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Tarifas', 'servicios' => $servicios, 'datos' => $tarifas];

    echo view ('main/panel_header');
    echo view ('tarifas/tarifas', $data);
    echo view ('main/msgeliminar'); //trae modal de confirm de eliminado
    echo view ('main/panel_footer');
  }

  public function eliminadas($activo = 0){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $servicios = $this->servicios->where('activo', 1)->findAll();

    $tarifas = $this->tarifas->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Tarifas anuladas', 'servicios' => $servicios, 'datos' => $tarifas];

    echo view ('main/panel_header');
    echo view ('tarifas/eliminadas', $data);
    echo view ('main/msgrestaurar'); //trae modal de confirm de restaurado
    echo view ('main/panel_footer');
  }

  public function nuevo(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $servicios = $this->servicios->where('activo', 1)->findAll();


    $data = ['titulo'=>'Agregar tarifa', 'servicios' => $servicios];
    echo view ('main/panel_header');
    echo view ('tarifas/nuevo', $data);
    echo view ('main/panel_footer');
  }

  public function insertar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->tarifas->save(['mes' => $this->request->getPost('mes'),
    'year' => $this->request->getPost('year'),
    'servicio' => $this->request->getPost('servicio'),
    'fecha_vencimiento' => $this->request->getPost('fecha_vencimiento'),
    'monto' => $this->request->getPost('monto'),
    'periodo' => date($this->request->getPost('year') . "-" . $this->request->getPost('mes') . "-01")
    ]);
    return redirect()->to(base_url().'/tarifas');

  }



  public function editar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $servicios = $this->servicios->where('activo', 1)->findAll();


    $tarifa = $this->tarifas->where('id', $id)->first();

    $data = ['titulo'=>'Editar tarifa','servicios' => $servicios, 'datos' => $tarifa];


    echo view ('main/panel_header');
    echo view ('tarifas/editar', $data);
    echo view ('main/panel_footer');
  }

  public function actualizar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->tarifas->update($this->request->getPost('id'),['mes' => $this->request->getPost('mes'),
    'year' => $this->request->getPost('year'),
    'servicio' => $this->request->getPost('servicio'),
    'fecha_vencimiento' => $this->request->getPost('fecha_vencimiento'),
    'monto' => $this->request->getPost('monto')
    ]);
    return redirect()->to(base_url().'/tarifas');

  }

  public function eliminar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->tarifas->update($id, ['activo' => 0]);
    return redirect()->to(base_url().'/tarifas');

  }



  public function restaurar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->tarifas->update($id, ['activo' => 1]);
    return redirect()->to(base_url().'/tarifas');

  }
}
