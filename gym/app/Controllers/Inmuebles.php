<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\InmueblesModel;
use App\Models\CategoriasModel;
use App\Models\ServiciosModel;
use App\Models\CiudadesModel;
use App\Models\MonedasModel;

class Inmuebles extends BaseController{

  protected $inmuebles, $session;

  public function __construct(){
    $this->inmuebles = new InmueblesModel();
    $this->categorias = new CategoriasModel();
    $this->servicios = new ServiciosModel();
    $this->ciudades = new CiudadesModel();
    $this->monedas = new MonedasModel();
    $this->session = session();

  }

  public function index($activo = 1){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $inmuebles = $this->inmuebles->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Inmuebles', 'datos' => $inmuebles];

    echo view ('main/panel_header');
    echo view ('inmuebles/inmuebles', $data);
    echo view ('main/msgeliminar'); //trae modal de confirm de eliminado
    echo view ('main/panel_footer');
  }

  public function eliminadas($activo = 0){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $inmuebles = $this->inmuebles->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Inmuebles eliminados', 'datos' => $inmuebles];

    echo view ('main/panel_header');
    echo view ('inmuebles/eliminadas', $data);
    echo view ('main/msgrestaurar'); //trae modal de confirm de restaurado
    echo view ('main/panel_footer');
  }

  public function nuevo(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $categorias = $this->categorias->where('activo', 1)->findAll();
    $servicios = $this->servicios->where('activo', 1)->findAll();
    $ciudades = $this->ciudades->where('activo', 1)->findAll();
    $monedas = $this->monedas->where('activo', 1)->findAll();

    $data = ['titulo'=>'Agregar inmueble', 'tipopropiedad' => $categorias,
            'tiposervicio' => $servicios, 'ciudades' => $ciudades,
            'monedas' => $monedas];
    echo view ('main/panel_header');
    echo view ('inmuebles/nuevo', $data);
    echo view ('main/panel_footer');
  }

  public function insertar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->inmuebles->save(['titulo' => $this->request->getPost('titulo'),
    'descripcion' => $this->request->getPost('descripcion'),
    'tipopropiedad' => $this->request->getPost('tipopropiedad'),
    'tiposervicio' => $this->request->getPost('tiposervicio'),
    'ciudad' => $this->request->getPost('ciudad'),
    'moneda' => $this->request->getPost('moneda'),
    'valor' => $this->request->getPost('valor'),
    'titular' => $this->request->getPost('titular'),
    'destacado' => $this->request->getPost('destacado')
    ]);
    return redirect()->to(base_url().'/inmuebles');

  }

  public function editar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $categorias = $this->categorias->where('activo', 1)->findAll();
    $servicios = $this->servicios->where('activo', 1)->findAll();
    $ciudades = $this->ciudades->where('activo', 1)->findAll();
    $monedas = $this->monedas->where('activo', 1)->findAll();
    $inmueble = $this->inmuebles->where('id', $id)->first();
    $data = ['titulo'=>'Editar inmueble', 'tipopropiedad' => $categorias,
            'tiposervicio' => $servicios, 'ciudades' => $ciudades,
            'monedas' => $monedas, 'inmueble' => $inmueble];
    echo view ('main/panel_header');
    echo view ('inmuebles/editar', $data);
    echo view ('main/panel_footer');
  }

  public function actualizar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->inmuebles->update($this->request->getPost('id'),['titulo' => $this->request->getPost('titulo'),
    'descripcion' => $this->request->getPost('descripcion'),
    'tipopropiedad' => $this->request->getPost('tipopropiedad'),
    'tiposervicio' => $this->request->getPost('tiposervicio'),
    'ciudad' => $this->request->getPost('ciudad'),
    'moneda' => $this->request->getPost('moneda'),
    'valor' => $this->request->getPost('valor'),
    'titular' => $this->request->getPost('titular'),
    'destacado' => $this->request->getPost('destacado')
    ]);
    return redirect()->to(base_url().'/inmuebles');

  }

  public function eliminar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->inmuebles->update($id, ['activo' => 0]);
    return redirect()->to(base_url().'/inmuebles');

  }

  public function restaurar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }
    
    $this->inmuebles->update($id, ['activo' => 1]);
    return redirect()->to(base_url().'/inmuebles');

  }
}
