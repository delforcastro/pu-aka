<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\PagosModel;
use App\Models\SuscripcionesModel;
use App\Models\ClientesModel;
use App\Models\TarifasModel;
use App\Models\ServiciosModel;


class Pagos extends BaseController{

  protected $pagos, $session;

  public function __construct(){
    $this->pagos = new PagosModel();
    $this->suscripciones = new SuscripcionesModel();
    $this->clientes = new ClientesModel();
    $this->tarifas = new TarifasModel();
    $this->servicios = new ServiciosModel();
    $this->session = session();


  }

  public function index($activo = 1){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    //$pagos = $this->pagos->where('activo', $activo)->findAll();
    $db = \Config\Database::connect();
    $builder = $db->table('gym_pagos p');
    $builder->select('p.id, p.suscripcion, p.monto, c.apellido, c.nombre, p.activo');
    $builder->join('suscripciones s', 'p.suscripcion=s.id');
    $builder->join('clientes c', 's.cliente=c.id');
    $builder->where('p.activo', $activo);
    $query = $builder->get();
    $pagos = $query->getResultArray();

    $admin = $this->session->admin;

    $data = ['titulo'=>'Pagos', 'datos' => $pagos, 'admin'=>$admin];

    echo view ('main/panel_header');
    echo view ('pagos/pagos', $data);
    echo view ('main/msgeliminar'); //trae modal de confirm de eliminado
    echo view ('main/panel_footer');
  }

  public function eliminadas($activo = 0){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $pagos = $this->pagos->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Pagos eliminados', 'datos' => $pagos];

    echo view ('main/panel_header');
    echo view ('pagos/eliminadas', $data);
    echo view ('main/msgrestaurar'); //trae modal de confirm de restaurado
    echo view ('main/panel_footer');
  }

  public function nuevo(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $suscripciones = $this->suscripciones->where('activo', 1)->findAll();

    $data = ['titulo'=>'Agregar pago', 'suscripciones'=>$suscripciones];
    echo view ('main/panel_header');
    echo view ('pagos/nuevo', $data);
    echo view ('main/panel_footer');
  }

  public function nuevopago(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $idCliente = $this->request->getPost('cliente');
    $idLiquidacion = $this->request->getPost('liquidacion');
    $monto = $this->request->getPost('monto');

    $cliente = $this->clientes->where('id', $idCliente)->first();
    $tarifa = $this->tarifas->where('id', $idLiquidacion)->first();
    $servicio = $this->servicios->where('id', $tarifa['servicio'])->first();
    $sus = $this->request->getPost('suscripcion');
    $suscripcion = $this->suscripciones->where('id', $sus) ->first();




    $data = ['titulo'=>'Nuevo pago', 'cliente' => $cliente, 'tarifa' => $tarifa, "servicio" => $servicio, "suscripcion" => $suscripcion];
    echo view ('main/panel_header');
    echo view ('pagos/nuevopago', $data);
    echo view ('main/panel_footer');
  }


  public function insertarpago(){
    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

      $this->pagos->save(['suscripcion' => $this->request->getPost('suscripcion'),
      'monto' => $this->request->getPost('monto'),
      'cliente' => $this->request->getPost('cliente'),
      'tarifa' => $this->request->getPost('tarifa'),
      'fecha_pago'=> $this->request->getPost('fecha_pago'),
      'usuario' => $this->session->nombre

      ]);

      return redirect()->to(base_url(). "/clientes/status/" . $this->request->getPost('cliente'));


  }


  public function insertar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

      $this->pagos->save(['suscripcion' => $this->request->getPost('suscripcion'),
      'monto' => $this->request->getPost('monto')
      ]);

      return redirect()->to(base_url().'/pagos');



  }

  public function editar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }
    $pago = $this->pagos->where('id', $id)->first();

    $data = ['titulo'=>'Editar pago', 'datos' => $pago];
    echo view ('main/panel_header');
    echo view ('pago/editar', $data);
    echo view ('main/panel_footer');
  }

  public function actualizar(){
    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->pagos->update($this->request->getPost('id'),['suscripcion' => $this->request->getPost('suscripcion'),
    'monto' => $this->request->getPost('monto')
    ]);
    return redirect()->to(base_url().'/pagos');

  }

  public function eliminar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->pagos->update($id, ['activo' => 0]);
    return redirect()->to(base_url().'/pagos');

  }

  public function restaurar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }


    $this->pagos->update($id, ['activo' => 1]);
    return redirect()->to(base_url().'/pagos');

  }
}
