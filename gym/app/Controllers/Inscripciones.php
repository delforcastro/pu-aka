<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\InscripcionesModel;
use App\Models\EventosModel;
use App\Models\DistanciasModel;
use App\Models\CategoriasModel;
use App\Models\ProvinciasModel;
use App\Models\TallesModel;

class Inscripciones extends BaseController{

  protected $inscripciones, $session;

  public function __construct(){
    $this->inscripciones = new InscripcionesModel();
    $this->eventos = new EventosModel();
    $this->distancias = new DistanciasModel();
    $this->categorias = new CategoriasModel();
    $this->provincias = new ProvinciasModel();
    $this->talles = new TallesModel();

    $this->session = session();

  }

  public function index($activo = 1){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $eventos = $this->eventos->where('activo', 1)->findAll();
    $distancias = $this->distancias->where('activo', 1)->findAll();
    $categorias = $this->categorias->where('activo', 1)->findAll();
    $provincias = $this->provincias->where('activo', 1)->findAll();
    $talles = $this->talles->where('activo', 1)->findAll();

    $inscripciones = $this->inscripciones->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Inscripciones', 'eventos' => $eventos,
            'distancias' => $distancias, 'categorias' => $categorias,
            'provincias' => $provincias, 'talles' => $talles, 'datos' => $inscripciones];

    echo view ('main/panel_header');
    echo view ('inscripciones/inscripciones', $data);
    echo view ('main/msgeliminar'); //trae modal de confirm de eliminado
    echo view ('main/panel_footer');
  }

  public function eliminadas($activo = 0){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $inscripciones = $this->inscripciones->where('activo', $activo)->findAll();
    $data = ['titulo'=>'Inscripciones eliminadas', 'datos' => $inscripciones];

    echo view ('main/panel_header');
    echo view ('inscripciones/eliminadas', $data);
    echo view ('main/msgrestaurar'); //trae modal de confirm de restaurado
    echo view ('main/panel_footer');
  }

  public function nuevo(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $eventos = $this->eventos->where('activo', 1)->findAll();
    $distancias = $this->distancias->where('activo', 1)->findAll();
    $categorias = $this->categorias->where('activo', 1)->findAll();
    $provincias = $this->provincias->where('activo', 1)->findAll();
    $talles = $this->talles->where('activo', 1)->findAll();

    $data = ['titulo'=>'Agregar inscripción', 'eventos' => $eventos,
            'distancias' => $distancias, 'categorias' => $categorias,
            'provincias' => $provincias, 'talles' => $talles];
    echo view ('main/panel_header');
    echo view ('inscripciones/nuevo', $data);
    echo view ('main/panel_footer');
  }

  public function insertar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->inscripciones->save(['evento' => $this->request->getPost('evento'),
    'distancia' => $this->request->getPost('distancia'),
    'apellido' => $this->request->getPost('apellido'),
    'nombre' => $this->request->getPost('nombre'),
    'dni' => $this->request->getPost('dni'),
    'sexo' => $this->request->getPost('sexo'),
    'fecha_nacimiento' => $this->request->getPost('fecha_nacimiento'),
    'categoria' => $this->request->getPost('categoria'),
    'telefono' => $this->request->getPost('telefono'),
    'ciudad' => $this->request->getPost('ciudad'),
    'provincia' => $this->request->getPost('provincia'),
    'talle' => $this->request->getPost('talle'),
    'email' => $this->request->getPost('email'),
    ]);
    return redirect()->to(base_url().'/inscripciones');

  }


  public function insertarweb(){


    $this->inscripciones->save(['evento' => $this->request->getPost('evento'),
    'distancia' => $this->request->getPost('distancia'),
    'apellido' => $this->request->getPost('apellido'),
    'nombre' => $this->request->getPost('nombre'),
    'dni' => $this->request->getPost('dni'),
    'sexo' => $this->request->getPost('sexo'),
    'fecha_nacimiento' => $this->request->getPost('fecha_nacimiento'),
    'categoria' => $this->request->getPost('categoria'),
    'telefono' => $this->request->getPost('telefono'),
    'ciudad' => $this->request->getPost('ciudad'),
    'provincia' => $this->request->getPost('provincia'),
    'talle' => $this->request->getPost('talle'),
    'email' => $this->request->getPost('email'),
    ]);
    #return redirect()->to(base_url());
    echo view('confirma');

    echo view ('footer');
  }

  public function editar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $eventos = $this->eventos->where('activo', 1)->findAll();
    $distancias = $this->distancias->where('activo', 1)->findAll();
    $categorias = $this->categorias->where('activo', 1)->findAll();
    $provincias = $this->provincias->where('activo', 1)->findAll();
    $talles = $this->talles->where('activo', 1)->findAll();


    $inscripcion = $this->inscripciones->where('id', $id)->first();

    $data = ['titulo'=>'Agregar inscripción', 'eventos' => $eventos,
            'distancias' => $distancias, 'categorias' => $categorias,
            'provincias' => $provincias, 'talles' => $talles,
          'inscripcion' => $inscripcion];


    echo view ('main/panel_header');
    echo view ('inscripciones/editar', $data);
    echo view ('main/panel_footer');
  }

  public function actualizar(){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->inscripciones->update($this->request->getPost('id'),['evento' => $this->request->getPost('evento'),
    'distancia' => $this->request->getPost('distancia'),
    'apellido' => $this->request->getPost('apellido'),
    'nombre' => $this->request->getPost('nombre'),
    'dni' => $this->request->getPost('dni'),
    'sexo' => $this->request->getPost('sexo'),
    'fecha_nacimiento' => $this->request->getPost('fecha_nacimiento'),
    'categoria' => $this->request->getPost('categoria'),
    'telefono' => $this->request->getPost('telefono'),
    'ciudad' => $this->request->getPost('ciudad'),
    'provincia' => $this->request->getPost('provincia'),
    'talle' => $this->request->getPost('talle'),
    'email' => $this->request->getPost('email'),
    ]);
    return redirect()->to(base_url().'/inscripciones');

  }

  public function eliminar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->inscripciones->update($id, ['activo' => 0]);
    return redirect()->to(base_url().'/inscripciones');

  }

  public function pagar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->inscripciones->update($id, ['pago' => 1]);
    return redirect()->to(base_url().'/inscripciones');

  }

  public function restaurar($id){

    if(!isset($this->session->id_usuario)){
      return redirect()->to(base_url() . "/login");
    }

    $this->inscripciones->update($id, ['activo' => 1]);
    return redirect()->to(base_url().'/inscripciones');

  }
}
