<?php

  namespace App\Models;
  use CodeIgniter\Model;

  class PagosModel extends Model
  {
    protected $table      = 'gym_pagos';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['suscripcion', 'monto', 'activo', 'cliente', 'tarifa', 'fecha_pago', 'usuario'];

    protected $useTimestamps = true;
    protected $createdField  = 'fecha_alta';
    protected $updatedField  = 'fecha_edicion';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
  }
 ?>
