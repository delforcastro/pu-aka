<?php
  // las categorias refieren a los tipos de inmuebles (casa, depto, lote, etc)
  namespace App\Models;
  use CodeIgniter\Model;

  class TarifasModel extends Model
  {
    protected $table      = 'gym_tarifas';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['mes', 'year', 'servicio', 'fecha_vencimiento', 'monto', 'activo', 'periodo'];

    protected $useTimestamps = true;
    protected $createdField  = 'fecha_alta';
    protected $updatedField  = 'fecha_edicion';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
  }
 ?>
