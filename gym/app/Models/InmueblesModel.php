<?php
  // las categorias refieren a los tipos de inmuebles (casa, depto, lote, etc)
  namespace App\Models;
  use CodeIgniter\Model;

  class InmueblesModel extends Model
  {
    protected $table      = 'items';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['titulo', 'descripcion', 'tipopropiedad', 'ciudad', 'tiposervicio', 'valor', 'moneda', 'activo', 'titular', 'destacado'];

    protected $useTimestamps = true;
    protected $createdField  = 'fecha_alta';
    protected $updatedField  = 'fecha_edicion';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
  }
 ?>
