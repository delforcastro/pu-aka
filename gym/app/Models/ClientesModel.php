<?php

  namespace App\Models;
  use CodeIgniter\Model;

  class ClientesModel extends Model
  {
    protected $table      = 'clientes';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['apellido','nombre', 'dni', 'grupo_sang', 'instagram', 'telefono', 'email', 'activo', 'fecha_nacimiento', 'gym_profesion', 'obs'];

    protected $useTimestamps = true;
    protected $createdField  = 'fecha_alta';
    protected $updatedField  = 'fecha_edicion';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
  }
 ?>
