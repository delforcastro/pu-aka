<?php

namespace App\Models;

use CodeIgniter\Model;

class LoginModel extends Model
{
  function admin_login($user, $pass)
  {
    $db = \Config\Database::connect();
    $builder = $db->table('admin_users');
    $query = $builder->select('user')
                      ->where('user', $user)
                      ->where('pss', $pass)
                      ->get();

    if ($query->getNumRows()> 0) {
      return true;
    }else{
      return false;
    }
  }
}
