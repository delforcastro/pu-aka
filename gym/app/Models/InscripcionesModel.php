<?php
  // las categorias refieren a los tipos de inmuebles (casa, depto, lote, etc)
  namespace App\Models;
  use CodeIgniter\Model;

  class InscripcionesModel extends Model
  {
    protected $table      = 'inscripciones';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['evento', 'distancia', 'apellido', 'nombre', 'dni', 'sexo', 'fecha_nacimiento',
                                'categoria', 'telefono', 'ciudad', 'provincia', 'talle',
                                'email', 'pago', 'monto'];

    protected $useTimestamps = true;
    protected $createdField  = 'fecha_alta';
    protected $updatedField  = 'fecha_edicion';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
  }
 ?>
