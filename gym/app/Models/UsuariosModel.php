<?php
  // las categorias refieren a los tipos de inmuebles (casa, depto, lote, etc)
  namespace App\Models;
  use CodeIgniter\Model;

  class UsuariosModel extends Model
  {
    protected $table      = 'usuarios';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['user', 'pss', 'nombre', 'activo', 'admin'];

    protected $useTimestamps = true;
    protected $createdField  = 'fecha_alta';
    protected $updatedField  = 'fecha_edicion';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
  }
 ?>
