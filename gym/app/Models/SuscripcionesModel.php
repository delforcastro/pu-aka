<?php
  // las categorias refieren a los tipos de inmuebles (casa, depto, lote, etc)
  namespace App\Models;
  use CodeIgniter\Model;

  class SuscripcionesModel extends Model
  {
    protected $table      = 'suscripciones';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['cliente', 'gym_servicio', 'activo', 'mes_alta', 'year_alta', 'fecha_suscripcion'];

    protected $useTimestamps = true;
    protected $createdField  = 'fecha_alta';
    protected $updatedField  = 'fecha_edicion';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
  }
 ?>
